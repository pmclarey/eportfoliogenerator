/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epg.view;

import epg.ComponentType;
import epg.model.Component;
import java.util.Map;
import java.util.TreeMap;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Patrick
 */
public class HeadingDialog extends AbstractDialog {
    
    Label headingInstructions;
    TextField headingTextField;
    
    public HeadingDialog() {
        this(new Component(ComponentType.HEADING));
    }
    
    public HeadingDialog(Component oldHeading) {
        super(150);
        Map headingMap = oldHeading.getProperties();
        headingInstructions = new Label("Enter the heading text:");
        headingTextField = new TextField(
                (String) headingMap.get(Component.TEXT));
        propertiesLayout.getChildren().addAll(headingInstructions,
                headingTextField);
        dialogStage.setTitle("Heading");
    }
    
    @Override
    void handleOK() {
        if (headingTextField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Heading text cannot be empty");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        Map newHeadingMap = new TreeMap<>();
        newHeadingMap.put(Component.TEXT, headingTextField.getText());
        component = new Component(ComponentType.HEADING);
        component.setProperties(newHeadingMap);
        dialogStage.hide();
    }
}