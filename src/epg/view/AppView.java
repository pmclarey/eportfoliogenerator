package epg.view;

import epg.ColorTemplate;
import epg.ComponentType;
import static epg.Constants.*;
import epg.Font;
import epg.Layout;
import epg.controller.FileToolBarController;
import epg.controller.ModeController;
import epg.controller.PageEditorController;
import epg.controller.SiteToolBarController;
import epg.model.Component;
import epg.model.EPortfolioModel;
import epg.model.ModelObserver;
import epg.model.PageModel;
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Patrick Clarey
 */
public class AppView implements ModelObserver {
    
    // application window and scene graph
    public Stage stage;
    Scene scene;
    
    // primary layout will be top to bottom
    VBox primaryLayout;
    
    // below, editing layout will be left to right
    HBox pageEditorLayout;
    
    // file tool bar handles new, load, save, save as, export, and exit
    ToolBar fileToolBar;
    Button newButton;
    Button loadButton;
    Button saveButton;
    Button saveAsButton;
    Button exportButton;
    Button exitButton;
    FileToolBarController fileToolBarController;
    
    // site tool bar handles adding, removing, and selecting pages in edit mode
    ToolBar siteToolBar;
    Button addButton;
    Button removeButton;
    List<ToggleButton> pageButtonList;
    ToggleGroup pageToggleGroup;
    PageModel selectedPage;
    SiteToolBarController siteToolBarController;
    
    // user selects page editing or viewing via TabPane
    TabPane workspaceModeTabPane;
    Tab pageEditorTab;
    Tab siteViewerTab;
    ModeController modeController;
    
    // page editor workspace
    VBox pageEditorWorkspace;
    Button changePropertiesButton;
    TextField titleTextField;
    ChoiceBox layoutChoiceBox;
    ChoiceBox colorTemplateChoiceBox;
    ChoiceBox fontChoiceBox;
    HBox componentEditPane;
    ScrollPane componentScrollPane;
    VBox componentListPane;
    List<RadioButton> componentButtonList;
    ToggleGroup componentToggleGroup;
    FlowPane modifyComponentPane;
    Button editComponentButton;
    Button removeComponentButton;
    FlowPane addComponentPane;
    Button addHeadingButton;
    Button addParagraphButton;
    Button addListButton;
    Button addImageButton;
    Button addVideoButton;
    Button addSlideShowButton;
    PageEditorController pageEditorController;
    
    // site viewer workspace
    WebView siteViewerWorkspace;
    WebEngine webEngine;
    URI tempURI;
    
    public AppView() {
    }
    
    public void initUI(Stage stage) {
        EPortfolioModel.registerObserver(this);
        this.stage = stage;
        initFileToolBar();
        initSiteToolBar();
        initPageEditorWorkspace();
        initSiteViewerWorkspace();
        initWorkspaceModeTabPane();
        initEventHandlers();
        initWindow();
    }
    
    public void loadPage(Toggle pageToLoadButton) {
        selectedPage = (PageModel) pageToLoadButton.getUserData();
        titleTextField.setText(selectedPage.getTitle());
        layoutChoiceBox.getSelectionModel().select(
                selectedPage.getLayout().ordinal());
        colorTemplateChoiceBox.getSelectionModel().select(
                selectedPage.getColorTemplate().ordinal());
        fontChoiceBox.getSelectionModel().select(
                selectedPage.getFont().ordinal());
        List<Component> componentList = selectedPage.getComponents();
        componentButtonList.clear();
        
        componentList.stream().forEach((component) -> {
            componentButtonList.add(getComponentButton(component));
        });
        
        componentToggleGroup.getToggles().clear();
        componentToggleGroup.getToggles().addAll(componentButtonList);
        componentListPane.getChildren().clear();
        componentListPane.getChildren().addAll(componentButtonList);
        updateControls();
    }
    
    public void loadWebEngine() {
        webEngine.load(tempURI.toString());
    }
    
    @Override
    public void processModelChanged() {
        updateControls();
        updateSiteToolBar();
        updatePageEditorWorkspace();
    }
    
    private void initFileToolBar() {
        newButton = getButton(LABEL_NEW, ICON_NEW);
        loadButton = getButton(LABEL_LOAD, ICON_LOAD);
        saveButton = getButton(LABEL_SAVE, ICON_SAVE);
        saveAsButton = getButton(LABEL_SAVE_AS, ICON_SAVE_AS);
        exportButton = getButton(LABEL_EXPORT, ICON_EXPORT);
        exitButton = getButton(LABEL_EXIT, ICON_EXIT);
        fileToolBar = new ToolBar(newButton, loadButton, saveButton,
                saveAsButton, exportButton, exitButton);
        fileToolBarController = new FileToolBarController(this);
    }
    
    private void initSiteToolBar() {
        addButton = getButton(LABEL_ADD, ICON_ADD_PAGE);
        removeButton = getButton(LABEL_REMOVE, ICON_REMOVE_PAGE);
        pageButtonList = new ArrayList<>();
        pageToggleGroup = new ToggleGroup();
        siteToolBar = new ToolBar(addButton, removeButton, new Separator());
        siteToolBar.setId(STYLE_ID_SITE_TOOL_BAR);
        siteToolBarController = new SiteToolBarController(this);
    }
    
    private void updateSiteToolBar() {
        EPortfolioModel ePortfolio = EPortfolioModel.getModel();
        List<PageModel> pageList = ePortfolio.getPages();
        pageButtonList.clear();
        
        pageList.stream().forEach((page) -> {
            pageButtonList.add(getPageButton(page.getTitle(), ICON_PAGE, page));
        });
        
        pageToggleGroup.getToggles().clear();
        pageToggleGroup.getToggles().addAll(pageButtonList);
        
        if (pageButtonList.size() == 1) {
            pageToggleGroup.selectToggle(pageButtonList.get(0));
        }
        
        siteToolBar.getItems().clear();
        siteToolBar.getItems().addAll(addButton, removeButton, new Separator());
        siteToolBar.getItems().addAll(pageButtonList);
    }
    
    private void initPageEditorWorkspace() {
        changePropertiesButton = new Button("Change ePortfolio Properties");
        Image propertiesImage = new Image(
                "file:" + PATH_ICONS + ICON_PROPERTIES); 
        changePropertiesButton.setGraphic(new ImageView(propertiesImage));
        changePropertiesButton.setPrefWidth(350);
       
        Label titleLabel = new Label("Enter a page title:");
        Label layoutLabel = new Label(LABEL_PAGE_LAYOUT);
        Label colorTemplateLabel = new Label(LABEL_PAGE_COLOR_TEMPLATE);
        Label fontLabel = new Label(LABEL_PAGE_FONT);
        
        titleTextField = new TextField();
        titleTextField.setMaxWidth(350);

        // populate choice box with layouts
        ObservableList<String> layoutList =
                FXCollections.observableArrayList();
        for (Layout layout : Layout.values()) {
            layoutList.add(layout.toString());
        }
        layoutChoiceBox = new ChoiceBox(layoutList);
        layoutChoiceBox.setMaxWidth(350);
       
        // populate choice box with color templates
        ObservableList<String> colorTemplateList =
                FXCollections.observableArrayList();
        for (ColorTemplate colorTemplate : ColorTemplate.values()) {
            colorTemplateList.add(colorTemplate.toString());
        }
        colorTemplateChoiceBox = new ChoiceBox(colorTemplateList);
        colorTemplateChoiceBox.setMinWidth(350);
       
        // populate choice box with fonts
        ObservableList<String> fontList =
                FXCollections.observableArrayList();
        for (Font font : Font.values()) {
            fontList.add(font.toString());
        }
        fontChoiceBox = new ChoiceBox(fontList);
        fontChoiceBox.setMinWidth(350);
        
        Label componentLabel = new Label("Page components:");
        componentLabel.setPadding(new Insets(20,0,5,0));
        componentListPane = new VBox();
        componentListPane.setSpacing(10.0);
        componentButtonList = new ArrayList<>();
        componentToggleGroup = new ToggleGroup();
        
        componentScrollPane = new ScrollPane(componentListPane);
        componentScrollPane.setMaxHeight(250.0);
        componentScrollPane.setPrefWidth(625.0);
        componentScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        
        editComponentButton = getButton("Edit", ICON_EDIT);
        removeComponentButton = getButton("Remove", ICON_REMOVE_COMPONENT);
        modifyComponentPane = new FlowPane(
                editComponentButton, removeComponentButton);
        modifyComponentPane.setOrientation(Orientation.VERTICAL);
        modifyComponentPane.setVgap(20.0);
        
        componentEditPane = new HBox(componentScrollPane, modifyComponentPane);
        componentEditPane.setSpacing(10);
        
        addHeadingButton = getButton("Add Heading", ICON_HEADING);
        addParagraphButton = getButton("Add Paragraph", ICON_PARAGRAPH);
        addListButton = getButton("Add List", ICON_LIST);
        addImageButton = getButton("Add Image", ICON_IMAGE);
        addVideoButton = getButton("Add Video", ICON_VIDEO);
        addSlideShowButton = getButton("Add Slide Show", ICON_SLIDE_SHOW);
        addComponentPane = new FlowPane(addHeadingButton, addParagraphButton,
                addListButton, addImageButton, addVideoButton,
                addSlideShowButton);
        addComponentPane.setHgap(5);
        addComponentPane.setPadding(new Insets(10,0,0,0));
        
        pageEditorWorkspace = new VBox(changePropertiesButton, titleLabel,
                titleTextField, layoutLabel, layoutChoiceBox,
                colorTemplateLabel, colorTemplateChoiceBox, fontLabel,
                fontChoiceBox, addComponentPane, componentLabel,
                componentEditPane);
        pageEditorWorkspace.setPadding(new Insets(20, 0, 0, 50));
        pageEditorWorkspace.setSpacing(5);
       
        pageEditorController = new PageEditorController(this);
       
        pageEditorLayout = new HBox(siteToolBar, pageEditorWorkspace);
    }
    
    private void updatePageEditorWorkspace() {
        if (selectedPage == null) return;
        
        List<Component> componentList = selectedPage.getComponents();
        componentButtonList.clear();
        
        componentList.stream().forEach((component) -> {
            componentButtonList.add(getComponentButton(component));
        });
        
        componentToggleGroup.getToggles().clear();
        componentToggleGroup.getToggles().addAll(componentButtonList);
        componentListPane.getChildren().clear();
        componentListPane.getChildren().addAll(componentButtonList);
    }
    
    private void initSiteViewerWorkspace() {
        siteViewerWorkspace = new WebView();
        webEngine = siteViewerWorkspace.getEngine();
        tempURI = new File(PATH_TEMP + INDEX_HTML).toURI();
    }
    
    private void initWorkspaceModeTabPane() {
        pageEditorTab = getTab(LABEL_EDITOR, ICON_EDITOR, pageEditorLayout);
        siteViewerTab = getTab(LABEL_VIEWER, ICON_VIEWER, siteViewerWorkspace);
        workspaceModeTabPane = new TabPane();
        workspaceModeTabPane.getTabs().addAll(pageEditorTab, siteViewerTab);
        modeController = new ModeController(this);
    }
    
    private void initEventHandlers() {
        newButton.setOnAction(e -> fileToolBarController.handleNew());
        loadButton.setOnAction(e -> fileToolBarController.handleLoad());
        saveButton.setOnAction(e -> fileToolBarController.handleSave());
        saveAsButton.setOnAction(e -> fileToolBarController.handleSaveAs());
        exportButton.setOnAction(e -> fileToolBarController.handleExport());
        exitButton.setOnAction(e -> fileToolBarController.handleExit());
        addButton.setOnAction(e -> siteToolBarController.handleAdd());
        removeButton.setOnAction(e ->
                siteToolBarController.handleRemove(selectedPage));
        changePropertiesButton.setOnAction(e -> 
                pageEditorController.handleChangeProperties());
        addHeadingButton.setOnAction(e ->
                pageEditorController.handleAddComponent(ComponentType.HEADING,
                    selectedPage));
        addParagraphButton.setOnAction(e ->
                pageEditorController.handleAddComponent(
                        ComponentType.PARAGRAPH, selectedPage));
        addListButton.setOnAction(e ->
                pageEditorController.handleAddComponent(ComponentType.LIST,
                        selectedPage));
        addImageButton.setOnAction(e ->
                pageEditorController.handleAddComponent(ComponentType.IMAGE,
                        selectedPage));
        addVideoButton.setOnAction(e ->
                pageEditorController.handleAddComponent(ComponentType.VIDEO,
                        selectedPage));
        addSlideShowButton.setOnAction(e ->
                pageEditorController.handleAddComponent(
                        ComponentType.SLIDE_SHOW, selectedPage));
        editComponentButton.setOnAction(e ->
                pageEditorController.handleEditComponent((Component)
                        componentToggleGroup.getSelectedToggle().getUserData(),
                        selectedPage));
        removeComponentButton.setOnAction(e ->
                pageEditorController.handleRemoveComponent((Component)
                        componentToggleGroup.getSelectedToggle().getUserData(),
                        selectedPage));
        siteViewerTab.setOnSelectionChanged(e -> 
                modeController.handleModeChanged(siteViewerTab.isSelected()));
        pageToggleGroup.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> observable, Toggle oldPage,
                        Toggle newPage) -> {
                    
                    siteToolBarController.handleSelect(newPage);
                }
        );
        componentToggleGroup.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> observable,
                        Toggle oldComponent, Toggle newComponent) -> {
                    
                    updateControls();
                }
        );
        titleTextField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldTitle,
                        String newTitle) -> {
                    
                    pageEditorController.handleChangeTitle(
                            newTitle, selectedPage);
                }
        );
        layoutChoiceBox.getSelectionModel().selectedIndexProperty().addListener(
                (ObservableValue<? extends Number> observable,
                        Number oldLayoutIndex, Number newLayoutIndex) -> {
                    
                    pageEditorController.handleChangeLayout(
                            Layout.values()[newLayoutIndex.intValue()],
                            selectedPage);
                }
        );
        colorTemplateChoiceBox.getSelectionModel().selectedIndexProperty().
                addListener((ObservableValue<? extends Number> observable,
                        Number oldIndex, Number newIndex) -> {
                    
                    pageEditorController.handleChangeColorTemplate(
                            ColorTemplate.values()[newIndex.intValue()],
                            selectedPage);
                }
        );
        fontChoiceBox.getSelectionModel().selectedIndexProperty().
                addListener((ObservableValue<? extends Number> observable,
                        Number oldIndex, Number newIndex) -> {
                    
                    pageEditorController.handleChangeFont(
                            Font.values()[newIndex.intValue()], selectedPage);
                }
        );
    }
    
    private void initWindow() {
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        stage.setWidth(1280);
        stage.setHeight(900);
        stage.centerOnScreen();
        workspaceModeTabPane.setPrefHeight(bounds.getHeight());
        primaryLayout = new VBox(fileToolBar, workspaceModeTabPane);
        scene = new Scene(primaryLayout);
        scene.getStylesheets().add(PATH_STYLES + STYLE_SHEET);
        stage.setScene(scene);
        stage.setTitle(APP_TITLE);
        stage.getIcons().add(new Image("file:" + PATH_ICONS + ICON_APP));
        updateControls();
        stage.show();
    }
    
    private void updateControls() {
            saveButton.setDisable(!EPortfolioModel.isLoaded()
                    || EPortfolioModel.isSavedSinceLastModified());
            saveAsButton.setDisable(!EPortfolioModel.isLoaded());
            exportButton.setDisable(!EPortfolioModel.isLoaded());
            workspaceModeTabPane.setVisible(EPortfolioModel.isLoaded());
            removeButton.setDisable(
                    pageToggleGroup.getSelectedToggle() == null ||
                            pageButtonList.size() < 2);
            editComponentButton.setDisable(
                    componentToggleGroup.getSelectedToggle() == null);
            removeComponentButton.setDisable(
                    componentToggleGroup.getSelectedToggle() == null);
    }
    
    static Button getButton(String text, String iconFileName) {
        Button button = new Button(text);
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        button.setGraphic(new ImageView(buttonImage));
        button.getStyleClass().add(STYLE_CLASS_TOOL_BAR_BUTTON);
        return button;
    }
    
    private ToggleButton getPageButton(
            String text, String iconFileName, PageModel page) {
        
        ToggleButton pageButton = new ToggleButton(text);
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image buttonImage = new Image(imagePath);
        pageButton.setGraphic(new ImageView(buttonImage));
        pageButton.getStyleClass().add(STYLE_CLASS_TOOL_BAR_BUTTON);
        pageButton.setUserData(page);
        return pageButton;
    }
    
    private RadioButton getComponentButton(Component component) {
        RadioButton componentButton = new RadioButton(component.toString());
        componentButton.setMaxWidth(600);
        componentButton.setTextOverrun(OverrunStyle.WORD_ELLIPSIS);
        componentButton.setUserData(component);
        return componentButton;
    }
    
    private Tab getTab(String text, String iconFileName, Node content) {
        Tab tab = new Tab(text);
        String imagePath = "file:" + PATH_ICONS + iconFileName;
        Image tabImage = new Image(imagePath);
        tab.setGraphic(new ImageView(tabImage));
        tab.setContent(content);
        tab.setClosable(false);
        return tab;
    }
    
    public ToggleGroup getPageToggleGroup() {
        return pageToggleGroup;
    }
    
    public void switchToViewer() {
        workspaceModeTabPane.getSelectionModel().select(siteViewerTab);
    }
    
    public void switchToEditor() {
        workspaceModeTabPane.getSelectionModel().select(pageEditorTab);
    }
}