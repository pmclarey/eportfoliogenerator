package epg.view;

import epg.controller.MediaSelector;
import epg.model.SlideModel;
import java.io.Serializable;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Window;

/**
 *
 * @author Patrick Clarey
 */
public class Slide extends HBox implements Serializable {
    
    private final SlideObserver OBSERVER;
    
    private final SlideModel model;
    
    // these are responsible for the slide image
    ImageView imageView;
    MediaSelector imageSelector;
    
    // these are responsible for the caption
    VBox captionBox;
    Label captionLabel;
    TextField captionTextField;
    
    public Slide(SlideObserver observer, Window ownerWindow) {
        this(observer, ownerWindow, new SlideModel());
    }
    
    public Slide(SlideObserver observer, Window ownerWindow, SlideModel model) {
        this.OBSERVER = observer;
        this.model = model;
        imageView = new ImageView(new Image(model.getImagePath()));
        imageView.setFitHeight(120);
        imageView.setFitWidth(220);
        imageSelector = new MediaSelector(ownerWindow);
        
        captionLabel = new Label("Caption:");
        captionTextField = new TextField(model.getCaption());
        
        captionBox = new VBox(captionLabel, captionTextField);
        captionBox.setPrefSize(350, 100);
        captionBox.setPadding(new Insets(33, 10, 33, 10));
        
        this.setStyle("-fx-border-color: black; -fx-border-width: 3;");
        this.getStyleClass().add("slide");
        
        initEventHandlers();
        
        getChildren().addAll(imageView, captionBox);
    }
    
    public SlideModel getModel() {
        return model;
    }
    
    public String getImagePath() {
        return model.getImagePath();
    }
    
    public String getCaption() {
        return captionTextField.getText();
    }
    
    public void setSelected(boolean selected) {
        if (selected) {
            setStyle("-fx-border-color: red; -fx-border-width: 3;");
        } else {
            setStyle("-fx-border-color: black; -fx-border-width: 3;");
        }
    }
    
    private void initEventHandlers() {
        imageView.setOnMouseClicked(e -> updateImage());
        this.setOnMouseClicked(e ->
                OBSERVER.processSelectedSlideChanged(this));
        captionTextField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldText,
                        String newText) -> {
                    
                    model.setCaption(newText);
                }
        );
    }
    
    private void updateImage() {
        String newImagePath = imageSelector.getImageFromUser();
        
        if (newImagePath == null) return;
        
        model.setImagePath("file:" + newImagePath);
        imageView.setImage(new Image(model.getImagePath()));
    }
}