package epg.view;

import epg.ComponentType;
import epg.controller.MediaSelector;
import epg.model.Component;
import java.util.Map;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.StringConverter;

/**
 *
 * @author Patrick Clarey
 */
public class ImageDialog extends AbstractDialog {
    
    // this holds the possible floating behaviors of an image
    private static final ObservableList<String> FLOAT_BEHAVIOR_LIST =
            FXCollections.observableArrayList(
            "Do not float", "Float to the left", "Float to the right");
    
    // these objects are associated with image selection
    String imagePath;
    ImageView imageView;
    MediaSelector imageSelector;
    
    // these objects are associated with the caption
    Label captionLabel;
    TextField captionTextField;
    
    // these objects are associated with image width/height
    Label widthLabel;
    Label heightLabel;
    Spinner widthSpinner;
    Spinner heightSpinner;
    
    // this selects floating behavior - none, left, or right
    Label floatLabel;
    ChoiceBox floatChoiceBox;
    
    public ImageDialog() {
        this(new Component(ComponentType.IMAGE));
        captionTextField.setDisable(true);
        widthSpinner.setDisable(true);
        heightSpinner.setDisable(true);
        floatChoiceBox.setDisable(true);
    }
    
    
    public ImageDialog(Component oldImage) {
        Map imageMap = oldImage.getProperties();
        imagePath = (String) imageMap.get(Component.SOURCE);
        Image image = new Image(imagePath);
        imageView = new ImageView(image);
        imageView.setFitHeight(300);
        imageView.setFitWidth(600);
        imageSelector = new MediaSelector(dialogStage);
        
        captionLabel = new Label("(Optional) Enter a caption:");
        captionTextField = new TextField((String) imageMap.get(Component.TEXT));
        
        widthLabel = new Label("Width:");
        heightLabel = new Label("Height:");
        
        // anonymous class to deal with pesky NumberFormatException
        StringConverter stringConverter = new StringConverter() {

            @Override
            public String toString(Object object) {
                try {
                    Double.parseDouble(object.toString());
                    return object.toString();
                } catch (NumberFormatException e) {
                    return "300.0";
                }
            }

            @Override
            public Object fromString(String string) {
                try {
                    return Double.parseDouble(string);
                } catch (NumberFormatException e) {
                    return 300.0;
                }
            }
        };
        
        double width = (Double) imageMap.get(Component.WIDTH);
        double height = (Double) imageMap.get(Component.HEIGHT);
        
        widthSpinner = new Spinner(
                0, Double.MAX_VALUE, width);
        widthSpinner.setEditable(true);
        widthSpinner.getValueFactory().setConverter(stringConverter);
        
        heightSpinner = new Spinner(
                0, Double.MAX_VALUE, height);
        heightSpinner.setEditable(true);
        heightSpinner.getValueFactory().setConverter(stringConverter);
        
        floatLabel = new Label("Indicate whether image should float:");
        floatChoiceBox = new ChoiceBox(FLOAT_BEHAVIOR_LIST);
        floatChoiceBox.setPrefWidth(150);
        floatChoiceBox.getSelectionModel().select(
                imageMap.get(Component.FLOAT));
        
        propertiesLayout.getChildren().addAll(imageView, captionLabel,
                captionTextField, widthLabel, widthSpinner, heightLabel,
                heightSpinner, floatLabel, floatChoiceBox);
        
        initEventHandlers();
        
        dialogStage.setTitle("Image");
    }
    
    private void initEventHandlers() {
        imageView.setOnMouseClicked(e -> updateImage());
    }
    
    private void updateImage() {
        String newImagePath = imageSelector.getImageFromUser();
        
        if (newImagePath == null) return;
        
        imagePath = "file:" + newImagePath;
        imageView.setImage(new Image(imagePath));
        widthSpinner.getValueFactory().setValue((double)
                imageView.getImage().getWidth());
        heightSpinner.getValueFactory().setValue((double)
                imageView.getImage().getHeight());
        captionTextField.setDisable(false);
        widthSpinner.setDisable(false);
        heightSpinner.setDisable(false);
        floatChoiceBox.setDisable(false);
    }
   
    @Override
    void handleOK() {
        if (captionTextField.isDisabled()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Please choose an image file.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        Map newImageMap = new TreeMap<>();
        newImageMap.put(Component.SOURCE , imagePath);
        newImageMap.put(Component.TEXT, captionTextField.getText());
        newImageMap.put(Component.WIDTH, widthSpinner.getValue());
        newImageMap.put(Component.HEIGHT, heightSpinner.getValue());
        newImageMap.put(Component.FLOAT, floatChoiceBox.getValue());
        component = new Component(ComponentType.IMAGE);
        component.setProperties(newImageMap);
        dialogStage.hide();
    }
}