package epg.view;

import static epg.Constants.ICON_CANCEL;
import static epg.Constants.ICON_OK;
import static epg.Constants.PATH_ICONS;
import static epg.Constants.PATH_STYLES;
import static epg.Constants.STYLE_SHEET;
import epg.model.Component;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Patrick Clarey
 */
abstract class AbstractDialog {
    
    private static double DEFAULT_HEIGHT = 700;
    
    // this represents the data for which the dialog box is responsible
    Component component;

    // window and scene graph for dialog boxes 
    Stage dialogStage;
    Scene dialogScene;
    
    // top to bottom layout for dialog boxes and property selectors
    VBox dialogLayout;
    VBox propertiesLayout;
    
    // buttons common to all dialog boxes, displayed bottom left to right
    HBox buttonLayout;
    Button cancelButton;
    Button okButton;

    AbstractDialog() {
        this(DEFAULT_HEIGHT);
    }
    
    AbstractDialog(double height) {
        cancelButton = new Button("Cancel");
        okButton = new Button("OK");
        cancelButton.setPrefSize(120, 40);
        okButton.setPrefSize(120, 40);
        cancelButton.setGraphic(new ImageView(
                new Image("file:" + PATH_ICONS + ICON_CANCEL)));
        okButton.setGraphic(new ImageView(
                new Image("file:" + PATH_ICONS + ICON_OK)));
        
        buttonLayout = new HBox(cancelButton, okButton);
        buttonLayout.setAlignment(Pos.CENTER);
        buttonLayout.setSpacing(100.0);
        buttonLayout.setPadding(new Insets(20.0, 0, 0, 0));
        
        propertiesLayout = new VBox();
        propertiesLayout.setSpacing(5);
        dialogLayout = new VBox(propertiesLayout, buttonLayout);
        dialogLayout.setPadding(new Insets(20.0, 100.0, 20.0, 100.0));
        
        dialogScene = new Scene(dialogLayout, 800.0, height);
        dialogScene.getStylesheets().add(PATH_STYLES + STYLE_SHEET);
        
        dialogStage = new Stage(StageStyle.UTILITY);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.setResizable(false);
        dialogStage.centerOnScreen();
        dialogStage.setScene(dialogScene);
        
        initEventHandlers();
    }
    
    private void initEventHandlers() {
        cancelButton.setOnAction(e -> handleCancel());
        okButton.setOnAction(e -> handleOK());
    }
    
    public Component getComponent() {
        dialogStage.showAndWait();
        return component;
    }
    
    void handleCancel() {
        component = null;
        dialogStage.hide();
    };
    
    abstract void handleOK();
}