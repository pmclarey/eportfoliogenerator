package epg.view;

import epg.ColorTemplate;
import epg.ComponentType;
import static epg.Constants.LABEL_BANNER_IMAGE;
import static epg.Constants.LABEL_DEFAULT_COLOR_TEMPLATE;
import static epg.Constants.LABEL_DEFAULT_FONT;
import static epg.Constants.LABEL_DEFAULT_LAYOUT;
import static epg.Constants.LABEL_FOOTER;
import static epg.Constants.LABEL_NAME;
import epg.Font;
import epg.Layout;
import epg.controller.MediaSelector;
import epg.model.Component;
import java.util.Map;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Patrick Clarey
 */
public class SettingsDialog extends AbstractDialog {
    
    private static final double COMPONENT_WIDTH = 600.0;
    
    // these objects are associated with the site banner image
    Label bannerImageInstructions;
    String bannerImagePath;
    ImageView bannerImageView;
    MediaSelector bannerImageSelector;
    
    // these objects are associated with the ePortfolio owner's name
    Label ownerNameInstructions;
    TextField ownerNameTextField;
    
    // these objects are associated with default layout, template, and font
    Label defaultLayoutInstructions;
    Label defaultColorTemplateInstructions;
    Label defaultFontInstructions;
    ChoiceBox layoutChoiceBox;
    ChoiceBox colorTemplateChoiceBox;
    ChoiceBox fontChoiceBox;
    
    // these objects are associated with a footer for each page
    Label footerInstructions;
    TextArea footerTextArea;
    
    public SettingsDialog() {
        this(new Component(ComponentType.SETTINGS));
    }
    
    public SettingsDialog(Component oldSettings) {
        Map settingsMap = oldSettings.getProperties();
        
        bannerImageInstructions = new Label(LABEL_BANNER_IMAGE);
        bannerImagePath = (String) settingsMap.get(
                Component.BANNER_IMAGE_PATH);
        bannerImageView = new ImageView(new Image(bannerImagePath));
        bannerImageView.setFitHeight(100.0);
        bannerImageView.setFitWidth(COMPONENT_WIDTH);
        bannerImageSelector = new MediaSelector(dialogStage);
        
        ownerNameInstructions = new Label(LABEL_NAME);
        ownerNameInstructions.setPadding(new Insets(20.0, 0, 0, 0));
        ownerNameTextField = new TextField((String) settingsMap.get(
                Component.NAME));
        ownerNameTextField.setMaxWidth(350.0);
        
        defaultLayoutInstructions = new Label(LABEL_DEFAULT_LAYOUT);
        defaultLayoutInstructions.setPadding(new Insets(20.0, 0, 0, 0));
        defaultColorTemplateInstructions =
                new Label(LABEL_DEFAULT_COLOR_TEMPLATE);
        defaultFontInstructions = new Label(LABEL_DEFAULT_FONT);
        
        int layoutIndex = ((Layout) settingsMap.get(
                Component.DEFAULT_LAYOUT)).ordinal();
        int colorTemplateIndex = ((ColorTemplate) settingsMap.get(
                Component.DEFAULT_COLOR_TEMPLATE)).ordinal();
        int fontIndex = ((Font) settingsMap.get(
                Component.DEFAULT_FONT)).ordinal();
        
        // populate choice box with layouts
        ObservableList<String> layoutList =
                FXCollections.observableArrayList();
        for (Layout layout : Layout.values()) {
            layoutList.add(layout.toString());
        }
        layoutChoiceBox = new ChoiceBox(layoutList);
        layoutChoiceBox.setMaxWidth(350.0);
        layoutChoiceBox.getSelectionModel().select(layoutIndex);
       
       
        // populate choice box with color templates
        ObservableList<String> colorTemplateList =
                FXCollections.observableArrayList();
        for (ColorTemplate colorTemplate : ColorTemplate.values()) {
            colorTemplateList.add(colorTemplate.toString());
        }
        colorTemplateChoiceBox = new ChoiceBox(colorTemplateList);
        colorTemplateChoiceBox.setMinWidth(350.0);
        colorTemplateChoiceBox.getSelectionModel().select(colorTemplateIndex);
        
        // populate choice box with fonts
        ObservableList<String> fontList =
                FXCollections.observableArrayList();
        for (Font font : Font.values()) {
            fontList.add(font.toString());
        }
        fontChoiceBox = new ChoiceBox(fontList);
        fontChoiceBox.setMinWidth(350.0);
        fontChoiceBox.getSelectionModel().select(fontIndex);
        
        footerInstructions = new Label(LABEL_FOOTER);
        footerInstructions.setPadding(new Insets(20.0, 0, 0, 0));
        footerTextArea = new TextArea(
                (String) settingsMap.get(Component.FOOTER));
        
        propertiesLayout.getChildren().addAll(
                bannerImageInstructions, bannerImageView, ownerNameInstructions,
                ownerNameTextField, defaultLayoutInstructions, layoutChoiceBox,
                defaultColorTemplateInstructions, colorTemplateChoiceBox,
                defaultFontInstructions, fontChoiceBox, footerInstructions,
                footerTextArea);
        
        initEventHandlers();
        
        dialogStage.setTitle("ePortfolio Properties");
    }
    
    private void initEventHandlers() {
        bannerImageView.setOnMouseClicked(e -> updateBannerImage());
    }
    
    private void updateBannerImage() {
        String newImagePath = bannerImageSelector.getImageFromUser();
        
        if (newImagePath == null) return;
        
        bannerImagePath = "file:" + newImagePath;
        bannerImageView.setImage(new Image(bannerImagePath));
    }
    
    @Override
    void handleOK() {
        if (ownerNameTextField.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Owner name cannot be empty");
            alert.showAndWait(); return;
        }
        
        int layoutIndex =
                layoutChoiceBox.getSelectionModel().getSelectedIndex();
        int colorTemplateIndex =
                colorTemplateChoiceBox.getSelectionModel().getSelectedIndex();
        int fontIndex =
                fontChoiceBox.getSelectionModel().getSelectedIndex();
        
        Map userSettings = new TreeMap<>();
        userSettings.put(Component.BANNER_IMAGE_PATH, bannerImagePath);
        userSettings.put(Component.NAME, ownerNameTextField.getText());
        userSettings.put(Component.DEFAULT_LAYOUT,
                Layout.values()[layoutIndex]);
        userSettings.put(Component.DEFAULT_COLOR_TEMPLATE,
                ColorTemplate.values()[colorTemplateIndex]);
        userSettings.put(Component.DEFAULT_FONT, Font.values()[fontIndex]);
        userSettings.put(Component.FOOTER, footerTextArea.getText());
        
        component = new Component(ComponentType.SETTINGS);
        component.setProperties(userSettings);
        dialogStage.hide();
    }
}