package epg.view;

import epg.ComponentType;
import static epg.Constants.ICON_ADD_ITEM;
import static epg.Constants.ICON_REMOVE_ITEM;
import static epg.Constants.PATH_ICONS;
import epg.model.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Patrick Clarey
 */
public class ListDialog extends AbstractDialog {
    
    // this is where user enters list item text
    Label itemLabel;
    TextField itemTextField;
    
    // these buttons add item or remove selected item
    FlowPane buttonPane;
    Button addItemButton;
    Button removeItemButton;
    
    // list of created items, can be selected for deletion
    List<String> itemList;
    ScrollPane itemsScrollPane;
    VBox itemsPane;
    ToggleGroup itemsToggleGroup;
    
    public ListDialog() {
        this(new Component(ComponentType.LIST));
    }
    
    public ListDialog(Component oldList) {
        super(500);
        itemList = (List) ((ArrayList)
                oldList.getProperties().get(Component.LIST_ITEMS)).clone();
        
        itemLabel = new Label("Enter list item:");
        itemTextField = new TextField();
        
        addItemButton = new Button("Add Item");
        String addItemImagePath = "file:" + PATH_ICONS + ICON_ADD_ITEM;
        addItemButton.setGraphic(new ImageView(new Image(addItemImagePath)));
        removeItemButton = new Button("Remove Item");
        String removeItemImagePath = "file:" + PATH_ICONS + ICON_REMOVE_ITEM;
        removeItemButton.setGraphic(new ImageView(
                new Image(removeItemImagePath)));
        
        buttonPane = new FlowPane(addItemButton, removeItemButton);
        buttonPane.setAlignment(Pos.CENTER);
        buttonPane.setHgap(20);
        
        itemsToggleGroup = new ToggleGroup();
        itemsPane = new VBox(10);
        itemsScrollPane = new ScrollPane(itemsPane);
        itemsScrollPane.setMinHeight(300);
        
        showItems();
        
        propertiesLayout.getChildren().addAll(itemLabel, itemTextField,
                buttonPane, itemsScrollPane);
        
        updateControls();
        initEventHandlers();
        
        dialogStage.setTitle("List");
    }
    
    private void showItems() {
        itemsPane.getChildren().clear();
        
        for (String itemText : itemList) {
            RadioButton rb = new RadioButton(itemText);
            rb.setUserData(itemText);
            rb.setToggleGroup(itemsToggleGroup);
            itemsPane.getChildren().add(rb);
        }
    }
    
    private void updateControls() {
        addItemButton.setDisable(itemTextField.getText().isEmpty());
        removeItemButton.setDisable(!
                itemsPane.getChildren().contains((Node)
                        itemsToggleGroup.getSelectedToggle()));
    }
    
    private void initEventHandlers() {
        addItemButton.setOnAction(e -> addItem());
        removeItemButton.setOnAction(e -> removeItem());
        
        
        itemTextField.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldText,
                        String newText) -> {
                    
                    updateControls();
                }
        );
        itemsToggleGroup.selectedToggleProperty().addListener(
                (ObservableValue<? extends Toggle> observable, Toggle oldToggle,
                        Toggle newToggle) -> {
                    
                    updateControls();
                }
        );
    }
    
    private void addItem() {
        itemList.add(itemTextField.getText());
        itemTextField.clear();
        showItems();
        updateControls();
    }
    
    private void removeItem() {
        itemList.remove(
                (String) itemsToggleGroup.getSelectedToggle().getUserData());
        showItems();
        updateControls();
    }

    @Override
    void handleOK() {
        if (itemList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "List must contain at least one item.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        Map newListMap = new TreeMap<>();
        newListMap.put(Component.LIST_ITEMS, itemList);
        component = new Component(ComponentType.LIST);
        component.setProperties(newListMap);
        dialogStage.hide();
    }
}