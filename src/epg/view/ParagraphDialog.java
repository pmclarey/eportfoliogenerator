package epg.view;

import epg.ComponentType;
import static epg.Constants.ICON_HYPERLINK;
import static epg.Constants.PATH_ICONS;
import epg.Font;
import epg.model.Component;
import epg.model.PageModel;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.StageStyle;

/**
 *
 * @author Patrick Clarey
 */
public class ParagraphDialog extends AbstractDialog {
    
    // list of supported font sizes
    private static final ObservableList<Integer> fontSizeList =
            FXCollections.observableArrayList(8, 9, 10, 11, 12, 14, 16, 18, 20,
            22, 24, 26, 28, 36, 48, 72);
    
    // controls associated with font and hyperlink selection
    GridPane fontControlLayout;
    Label fontLabel;
    Label fontSizeLabel;
    ChoiceBox fontChoiceBox;
    ComboBox fontSizeComboBox;
    Button addHyperlinkButton;
    
    // controls associated with paragraph text
    Label paragraphLabel;
    TextArea paragraphTextArea;
    
    public ParagraphDialog(PageModel page) {
        this(new Component(ComponentType.PARAGRAPH));
        fontChoiceBox.getSelectionModel().select(page.getFont().ordinal());
    }
    
    public ParagraphDialog(Component oldParagraph) {
        super(400);
        Map paraMap = oldParagraph.getProperties();
        fontLabel = new Label("Font:");
        fontSizeLabel = new Label("Size:");
        
        // populate choice box with fonts
        ObservableList<String> fontList =
                FXCollections.observableArrayList();
        
        for (Font font : Font.values()) {
            fontList.add(font.toString());
        }
        
        int fontIndex = ((Font) paraMap.get(Component.FONT)).ordinal();
         
        fontChoiceBox = new ChoiceBox(fontList);
        fontChoiceBox.setMinWidth(350);
        fontChoiceBox.getSelectionModel().select(fontIndex);
        
        // populate choice box with font sizes
        fontSizeComboBox = new ComboBox(fontSizeList);
        fontSizeComboBox.setValue(paraMap.get(Component.FONT_SIZE));
        
        addHyperlinkButton = new Button("Insert Hyperlink");
        String hyperlinkImagePath = "file:" + PATH_ICONS + ICON_HYPERLINK;
        addHyperlinkButton.setGraphic(new ImageView(
                new Image(hyperlinkImagePath)));
        addHyperlinkButton.setDisable(true);
        
        fontControlLayout = new GridPane();
        fontControlLayout.setHgap(10);
        
        fontControlLayout.add(fontLabel, 0, 0);
        fontControlLayout.add(fontSizeLabel, 1, 0);
        fontControlLayout.add(fontChoiceBox, 0, 1);
        fontControlLayout.add(fontSizeComboBox, 1, 1);
        fontControlLayout.add(addHyperlinkButton, 2, 1);
        
        paragraphLabel = new Label("Enter paragraph text:");
        paragraphLabel.setPadding(new Insets(20,0,0,0));
        paragraphTextArea = new TextArea((String) paraMap.get(Component.TEXT));
        
        propertiesLayout.getChildren().addAll(fontControlLayout, paragraphLabel,
                paragraphTextArea);
        
        initEventHandlers();
        
        dialogStage.setTitle("Paragraph");
    }
    
    private void initEventHandlers() {
        paragraphTextArea.selectedTextProperty().addListener(
                (ObservableValue<? extends String> observable, String oldText,
                        String newText) -> {
                    
            addHyperlinkButton.setDisable(
                    paragraphTextArea.getSelectedText().isEmpty());
        });
        addHyperlinkButton.setOnAction(e -> addHyperlink());
    }
    
    private void addHyperlink() {
        TextInputDialog urlDialog = new TextInputDialog();
        urlDialog.initStyle(StageStyle.UTILITY);
        urlDialog.initModality(Modality.APPLICATION_MODAL);
        urlDialog.setTitle("Add Hyperlink");
        urlDialog.setHeaderText("");
        urlDialog.setContentText("Enter URL:");
        
        Optional<String> url = urlDialog.showAndWait();
        if (!url.isPresent()) {
            return;
        }
        
        String selectedText = paragraphTextArea.getSelectedText();
        String openingTag = "<a href=\"" + url.get() + "\">";
        String closingTag = "</a>";
        
        paragraphTextArea.replaceSelection(
                openingTag + selectedText + closingTag);
    }
    
    @Override
    void handleOK() {
        if (paragraphTextArea.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Paragraph text cannot be empty");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        Map newParaMap = new TreeMap<>();
        newParaMap.put(Component.FONT, Font.values()[
                fontChoiceBox.getSelectionModel().getSelectedIndex()]);
        newParaMap.put(Component.FONT_SIZE,
                fontSizeComboBox.getSelectionModel().getSelectedItem());
        newParaMap.put(Component.TEXT, paragraphTextArea.getText());
        component = new Component(ComponentType.PARAGRAPH);
        component.setProperties(newParaMap);
        dialogStage.hide();
    }
}