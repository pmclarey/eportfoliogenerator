package epg.view;

import epg.ComponentType;
import static epg.Constants.ICON_VIDEO;
import static epg.Constants.PATH_ICONS;
import epg.controller.MediaSelector;
import epg.model.Component;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.util.StringConverter;

/**
 *
 * @author Patrick Clarey
 */
public class VideoDialog extends AbstractDialog {
    
    // these change and display the video
    String videoPath;
    Button loadVideoButton;
    MediaView video;
    MediaSelector videoSelector;
    
    // these select the caption
    Label captionLabel;
    TextField captionTextField;
    
    // these select width and height
    Label widthLabel;
    Label heightLabel;
    Spinner widthSpinner;
    Spinner heightSpinner;
    
    public VideoDialog() {
        this(new Component(ComponentType.VIDEO));
        captionTextField.setDisable(true);
        widthSpinner.setDisable(true);
        heightSpinner.setDisable(true);
    }
    
    public VideoDialog(Component oldVideo) {
        super(600);
        
        loadVideoButton = new Button("Load Video");
        String loadVideoImagePath = "file:" + PATH_ICONS + ICON_VIDEO;
        loadVideoButton.setGraphic(new ImageView(
                new Image(loadVideoImagePath)));
        videoSelector = new MediaSelector(dialogStage);
        
        Map videoMap = oldVideo.getProperties();
        videoPath = ((String) videoMap.get(Component.SOURCE));
        Media media = new Media(new File(videoPath).toURI().toASCIIString());
        video = new MediaView(new MediaPlayer(media));
        video.setFitHeight(300);
        video.setFitWidth(800);
        video.getMediaPlayer().setAutoPlay(true);
        
        captionLabel = new Label("(Optional) Enter a caption:");
        captionTextField = new TextField((String) videoMap.get(Component.TEXT));
        captionTextField.setMaxWidth(535);
        
        widthLabel = new Label("Width:");
        heightLabel = new Label("Height:");
        
        // anonymous class to deal with pesky NumberFormatException
        StringConverter stringConverter = new StringConverter() {

            @Override
            public String toString(Object object) {
                try {
                    Double.parseDouble(object.toString());
                    return object.toString();
                } catch (NumberFormatException e) {
                    return "500.0";
                }
            }

            @Override
            public Object fromString(String string) {
                try {
                    return Double.parseDouble(string);
                } catch (NumberFormatException e) {
                    return 300.0;
                }
            }
        };
        
        double width = (Double) videoMap.get(Component.WIDTH);
        double height = (Double) videoMap.get(Component.HEIGHT);
        
        widthSpinner = new Spinner(0, Double.MAX_VALUE, width);
        widthSpinner.setEditable(true);
        widthSpinner.getValueFactory().setConverter(stringConverter);
        heightSpinner = new Spinner(0, Double.MAX_VALUE, height);
        heightSpinner.setEditable(true);
        heightSpinner.getValueFactory().setConverter(stringConverter);
        
        propertiesLayout.getChildren().addAll(loadVideoButton, video,
                captionLabel, captionTextField, widthLabel, widthSpinner,
                heightLabel, heightSpinner);
        
        initEventHandlers();
        
        dialogStage.setTitle("Video");
    }
    
    private void initEventHandlers() {
        loadVideoButton.setOnAction(e -> updateVideo());
    }
    
    private void updateVideo() {
        String newVideoPath = videoSelector.getVideoFromUser();
        
        if (newVideoPath == null) return;
        
        videoPath = newVideoPath;
        Media media = new Media(new File(videoPath).toURI().toASCIIString());
        video.setMediaPlayer(new MediaPlayer(media));
        widthSpinner.getValueFactory().setValue(800.0);
        heightSpinner.getValueFactory().setValue(300.0);
        video.getMediaPlayer().setAutoPlay(true);
        captionTextField.setDisable(false);
        widthSpinner.setDisable(false);
        heightSpinner.setDisable(false);
    }

    @Override
    void handleOK() {
        if (captionTextField.isDisabled()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Please choose a video file.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        Map newVideoMap = new TreeMap<>();
        newVideoMap.put(Component.SOURCE , videoPath);
        newVideoMap.put(Component.TEXT, captionTextField.getText());
        newVideoMap.put(Component.WIDTH, widthSpinner.getValue());
        newVideoMap.put(Component.HEIGHT, heightSpinner.getValue());
        component = new Component(ComponentType.VIDEO);
        component.setProperties(newVideoMap);
        dialogStage.hide();
    }
}