package epg.view;

import epg.ComponentType;
import static epg.Constants.ICON_ADD_ITEM;
import static epg.Constants.ICON_MOVE_DOWN;
import static epg.Constants.ICON_MOVE_UP;
import static epg.Constants.ICON_REMOVE_ITEM;
import static epg.Constants.IMAGE_DEFAULT;
import static epg.Constants.PATH_IMAGES;
import epg.model.Component;
import epg.model.SlideModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Patrick Clarey
 */
public class SlideShowDialog extends AbstractDialog implements SlideObserver {

    // data structure for slide show
    ObservableList<Slide> slideList;
    
    // keep track of which slide is selected
    Slide selectedSlide;

    // tool bar for editing slide show
    FlowPane slideShowToolBar;
    Button addSlideButton;
    Button removeSlideButton;
    Button moveSlideUpButton;
    Button moveSlideDownButton;
    
    // pane for displaying, selecting slides
    ScrollPane slideShowScrollPane;
    VBox slideShowPane;
    
    public SlideShowDialog() {
        this(new Component(ComponentType.SLIDE_SHOW));
    }
    
    public SlideShowDialog(Component oldSlideShow) {
        super(600);
        slideList = FXCollections.observableArrayList();
        List<SlideModel> oldModels = (List<SlideModel>)
                oldSlideShow.getProperties().get(Component.SLIDES);
        
        for (SlideModel model : oldModels) {
            Slide slide = new Slide(this, dialogStage, model);
            slideList.add(slide);
        }
        
        selectedSlide = null;
         
        addSlideButton = AppView.getButton("Add Slide", ICON_ADD_ITEM);
        removeSlideButton = AppView.getButton("Remove Slide", ICON_REMOVE_ITEM);
        moveSlideUpButton = AppView.getButton("Move Up", ICON_MOVE_UP);
        moveSlideDownButton = AppView.getButton("Move Down", ICON_MOVE_DOWN);
        
        slideShowToolBar = new FlowPane(addSlideButton, removeSlideButton,
                moveSlideUpButton, moveSlideDownButton);
        slideShowToolBar.setAlignment(Pos.CENTER);
        slideShowToolBar.setHgap(20);
        
        slideShowPane = new VBox(10);
        slideShowPane.setStyle("-fx-background-color:#e3f2fd;");
        slideShowPane.setStyle("-fx-padding: 0px, 0px, 0px, 0px;");
        slideShowScrollPane = new ScrollPane(slideShowPane);
        slideShowScrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        slideShowScrollPane.setStyle("-fx-background-color:#e3f2fd;");
        slideShowScrollPane.setStyle("-fx-padding:0px 0px 0px 0px;");
        slideShowScrollPane.setPrefHeight(400);
        
        propertiesLayout.getChildren().addAll(
                slideShowToolBar, slideShowScrollPane);
        
        initEventHandlers();
        showSlides();
        dialogStage.setTitle("Slide Show");
    }
    
    private void initEventHandlers() {
        addSlideButton.setOnAction(e -> addSlide());
        removeSlideButton.setOnAction(e -> removeSlide());
        moveSlideDownButton.setOnAction(e -> moveSlideDown());
        moveSlideUpButton.setOnAction(e -> moveSlideUp());
    }
    
    private void addSlide() {
        Slide slide = new Slide(this, dialogStage);
        slideList.add(slide);
        showSlides();
    }
    
    private void removeSlide() {
        slideList.remove(selectedSlide);
        selectedSlide = null;
        showSlides();
    }
    
    private void moveSlideDown() {
        int selectedSlideIndex = slideList.indexOf(selectedSlide);
        Slide temp = slideList.get(selectedSlideIndex + 1);
        slideList.set(selectedSlideIndex + 1, selectedSlide);
        slideList.set(selectedSlideIndex, temp);
        showSlides();
    }
    
    private void moveSlideUp() {
        int selectedSlideIndex = slideList.indexOf(selectedSlide);
        Slide temp = slideList.get(selectedSlideIndex - 1);
        slideList.set(selectedSlideIndex - 1, selectedSlide);
        slideList.set(selectedSlideIndex, temp);
        showSlides();
    }
    
    private void showSlides() {
        slideShowPane.getChildren().clear();
        slideShowPane.getChildren().addAll(slideList);
        updateControls();
    }
    
    private void updateControls() {
        removeSlideButton.setDisable(selectedSlide == null);
        moveSlideDownButton.setDisable(selectedSlide == null ||
                selectedSlide == slideList.get(slideList.size() - 1));
        moveSlideUpButton.setDisable(selectedSlide == null ||
                selectedSlide == slideList.get(0));
    }
    
    @Override
    void handleOK() {
        if (slideList.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "A slide show must have at least one slide.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        for (Slide slide : slideList) {
            if (slide.getImagePath().equals("file:" + PATH_IMAGES + IMAGE_DEFAULT)) {
                Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Each slide must have an image.");
                alert.setHeaderText("");
                alert.showAndWait(); return;
            }
        }
        
        Map newSlideShowMap = new TreeMap<>();
        List<SlideModel> slides = new ArrayList<>();
        for (Slide slide : slideList) {
            slides.add(slide.getModel());
        }
        
        newSlideShowMap.put(Component.SLIDES, slides);
        component = new Component(ComponentType.SLIDE_SHOW);
        component.setProperties(newSlideShowMap);
        dialogStage.hide();
    }   

    @Override
    public void processSelectedSlideChanged(Slide slide) {
        if (selectedSlide != null) {
            selectedSlide.setSelected(false);
        }
        
        selectedSlide = slide;
       
        if (selectedSlide != null) {
            selectedSlide.setSelected(true);
        }
        
        updateControls();
    }
}