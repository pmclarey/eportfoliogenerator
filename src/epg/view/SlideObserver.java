package epg.view;

/**
 *
 * @author Patrick Clarey
 */
public interface SlideObserver {
    abstract void processSelectedSlideChanged(Slide slide);
}