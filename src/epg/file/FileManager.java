package epg.file;

import epg.model.EPortfolioModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Modality;
import javafx.stage.StageStyle;

/**
 *
 * @author Patrick Clarey
 */
public class FileManager {
    public static final String PATH_SAVED = "./saved/";
    public static final String EXT_EPF = ".epf";
    
    public static String getTitleFromUser() {
        String title;
        
        TextInputDialog titleDialog = new TextInputDialog();
        titleDialog.initStyle(StageStyle.UTILITY);
        titleDialog.initModality(Modality.APPLICATION_MODAL);
        titleDialog.setTitle("Save ePortfolio");
        titleDialog.setHeaderText("");
        titleDialog.setContentText("Enter file name:");
        
        do {
            Optional<String> userInput = titleDialog.showAndWait();
        
            if (!userInput.isPresent()) {
                return null;
            }
        
            title = userInput.get();
        
            if (title.equals("")) {
                Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Title cannot be blank.");
                alert.setHeaderText("");
                alert.showAndWait();
            }
        } while (title.equals(""));
        
        EPortfolioModel.getModel().setTitle(title);
        return title;
    }
    
    public static void save(String title) throws IOException {
        EPortfolioModel portfolio = EPortfolioModel.getModel();
        String path = PATH_SAVED + title + EXT_EPF;
        FileOutputStream fileOut = new FileOutputStream(path);
        ObjectOutputStream objOut = new ObjectOutputStream(fileOut);
        objOut.writeObject(portfolio);
        objOut.close();
        fileOut.close();
        EPortfolioModel.markSaved();
    }
    
    public static void load(File fileToLoad)
            throws IOException, ClassNotFoundException {
        
        FileInputStream fileIn = new FileInputStream(fileToLoad);
        ObjectInputStream objIn = new ObjectInputStream(fileIn);
        EPortfolioModel portfolio = (EPortfolioModel) objIn.readObject();
        objIn.close();
        fileIn.close();
        EPortfolioModel.loadModel(portfolio);
    }
}
