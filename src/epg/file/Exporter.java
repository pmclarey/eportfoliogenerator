package epg.file;

import epg.ColorTemplate;
import epg.Layout;
import epg.model.Component;
import epg.model.EPortfolioModel;
import epg.model.PageModel;
import epg.model.SlideModel;
import epg.view.Slide;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author Patrick Clarey
 */
public class Exporter {
    
    public static final String JSON_NAME = "name";
    public static final String JSON_BANNER_IMAGE_PATH = "bannerImagePath";
    public static final String JSON_LINKS = "links";
    public static final String JSON_URL = "URL";
    public static final String JSON_TITLE = "title";
    public static final String JSON_STYLE_SHEETS = "stylesheets";
    public static final String JSON_COMPONENTS = "components";
    public static final String JSON_FOOTER = "footer";
    public static final String JSON_TYPE = "type";
    public static final String JSON_TEXT = "text";
    public static final String JSON_FONT = "font";
    public static final String JSON_FONT_SIZE = "fontSize";
    public static final String JSON_ITEMS = "items";
    public static final String JSON_SRC = "src";
    public static final String JSON_WIDTH = "width";
    public static final String JSON_HEIGHT = "height";
    public static final String JSON_FLOAT = "float";
    public static final String JSON_SLIDES = "slides";
    public static final String JSON_IMAGE_PATH = "imagePath";
    public static final String JSON_CAPTION = "caption";
    
    public static final String DIR_DATA = "./data/";
    public static final String DIR_JS = "./js/";
    public static final String DIR_STYLES = "./styles/";
    public static final String DIR_IMAGES = "./images/";
    public static final String DIR_VIDEOS = "./videos/";
    
    public static final String PATH_WEB_FILES = "./web-files/";
    public static final String BASE_HTML = "base.html";
    public static final String INDEX_HTML = "index.html";
    public static final String PAGE = "page";
    public static final String HTML_EXT = ".html";
    public static final String JS_FILE = "EPortfolio.js";
    public static final String LAYOUT_BASE = "layout_base.css";
    
    private static String destination;
    private static Map<String, Object> properties;
    private static JsonWriterFactory writerFactory;
    
    public static void export(String destination) throws IOException {
        Exporter.destination = destination;
        Exporter.properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        createDirectories();
        copyHtml(destination);
        copyJS(destination + DIR_JS);
        copyStyles(destination + DIR_STYLES);
        copyButtonImages();
        writeGlobalJson(destination + DIR_DATA + "global.json");
        writePagesJson(destination + DIR_DATA);
    }
    
    private static void createDirectories()
            throws IOException {
        
        deleteRecursive(new File(destination));
        
        final Path PATH_DATA = Paths.get(destination + DIR_DATA);
        final Path PATH_JS = Paths.get(destination + DIR_JS);
        final Path PATH_STYLES = Paths.get(destination + DIR_STYLES);
        final Path PATH_IMAGES = Paths.get(destination + DIR_IMAGES);
        final Path PATH_VIDEOS = Paths.get(destination + DIR_VIDEOS);
        Files.createDirectories(PATH_DATA);
        Files.createDirectories(PATH_JS);
        Files.createDirectories(PATH_STYLES);
        Files.createDirectories(PATH_IMAGES);
        Files.createDirectories(PATH_VIDEOS);
    }
    
    private static void deleteRecursive(File path) {
        if (path.isDirectory()) {
            
            for (File file : path.listFiles()) {
                deleteRecursive(file);
            }
        }
        
        path.delete();
    }
    
    private static void copyHtml(String path) throws IOException {
        final Path SOURCE = Paths.get(PATH_WEB_FILES + BASE_HTML);
        int numPages = EPortfolioModel.getModel().getPages().size();
        
        for (int i = 1; i <= numPages; i++) {
            String dest = (i == 1)
                    ? path + INDEX_HTML
                    : path + PAGE + i + HTML_EXT;
            
            Path target = Paths.get(dest);
            Files.copy(SOURCE, target);
        }
    }
    
    private static void copyJS(String path) throws IOException {
        final Path SOURCE = Paths.get(PATH_WEB_FILES + JS_FILE);
        final Path TARGET = Paths.get(path + JS_FILE);
        Files.copy(SOURCE, TARGET);
    }
    
    private static void copyStyles(String path) throws IOException {
        Path source = Paths.get(PATH_WEB_FILES + LAYOUT_BASE);
        Path target = Paths.get(path + LAYOUT_BASE);
        Files.copy(source, target);
        
        for (Layout layout : Layout.values()) {
            String fileName = layout.getStyleSheet();
            source = Paths.get(PATH_WEB_FILES + fileName);
            target = Paths.get(path + fileName);
            Files.copy(source, target);
        }
        
        for (ColorTemplate colorTemp : ColorTemplate.values()) {
            String fileName = colorTemp.getStyleSheet();
            source = Paths.get(PATH_WEB_FILES + fileName);
            target = Paths.get(path + fileName);
            Files.copy(source, target);
        }
    }
    
    private static void copyButtonImages() throws IOException {
        copyImage(PATH_WEB_FILES + "next.png", "next.png");
        copyImage(PATH_WEB_FILES + "pause.png", "pause.png");
        copyImage(PATH_WEB_FILES + "play.png", "play.png");
        copyImage(PATH_WEB_FILES + "previous.png", "previous.png");
    }
    
    private static void writeGlobalJson(String filePath) throws IOException {
        OutputStream out = new FileOutputStream(filePath);
        JsonWriter jsonWriter = writerFactory.createWriter(out);
        
        Map<String, Object> settings = (Map<String, Object>)
                EPortfolioModel.getModel().getSettings().getProperties();
        String name = (String) settings.get(Component.NAME);
        String bannerImagePath = (String) settings.get(
                Component.BANNER_IMAGE_PATH);
        String bannerImageFileName = bannerImagePath;
        
        if (bannerImagePath.indexOf('\\') != 0) {
            bannerImageFileName = bannerImageFileName.substring(
                    bannerImagePath.lastIndexOf('\\') + 1);
        }
        if (bannerImagePath.indexOf('/') != 0) {
            bannerImageFileName = bannerImageFileName.substring(
                    bannerImagePath.lastIndexOf('/') + 1);
        }
        
        copyImage(bannerImagePath, bannerImageFileName);
        
        JsonArray links = makeLinksArray(EPortfolioModel.getModel().getPages());
        
        JsonObject global = Json.createObjectBuilder()
                .add(JSON_NAME, name)
                .add(JSON_BANNER_IMAGE_PATH, DIR_IMAGES + bannerImageFileName)
                .add(JSON_LINKS, links)
                .add(JSON_FOOTER, (String) settings.get(Component.FOOTER))
                .build();
        
        jsonWriter.writeObject(global);
    }
    
    private static void copyImage(String uri, String fileName)
            throws IOException {
        
        String path = uri.substring(uri.indexOf(':') + 1);
        Path source = Paths.get(path);
        Path target = Paths.get(destination + DIR_IMAGES + fileName);
        Files.copy(source, target, REPLACE_EXISTING);
    }
    
    private static JsonArray makeLinksArray(List<PageModel> pages) {
        int i = 1;
        JsonArrayBuilder jab = Json.createArrayBuilder();
        for (PageModel page : pages) {
            JsonObject jso = makeLinkObject(page, i++);
            jab.add(jso);
        }
        
        return jab.build();
    }
    
    private static JsonObject makeLinkObject(PageModel page, int pageIndex) {
        String url = (pageIndex == 1)
                ? INDEX_HTML
                : PAGE + pageIndex + HTML_EXT;
        
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_URL, url)
                .add(JSON_TITLE, page.getTitle())
                .build();
        
        return jso;
    }
    
    private static void writePagesJson(String path) throws IOException {
        List<PageModel> pages = EPortfolioModel.getModel().getPages();
        writePage(pages.get(0), path, "index.json");
        
        for (int i = 1; i < pages.size(); i++) {
            writePage(pages.get(i), path, "page" + (i + 1) + ".json");
        }
    }
    
    private static void writePage(PageModel page, String path, String fileName)
            throws IOException{
        
        OutputStream out = new FileOutputStream(path + fileName);
        JsonWriter jsonWriter = writerFactory.createWriter(out);
        JsonArray stylesheets = makeStyleSheetsArray(page);
        JsonArray components = makeComponentsArray(page);
        
        JsonObject pageObject = Json.createObjectBuilder()
                .add(JSON_TITLE, page.getTitle())
                .add(JSON_STYLE_SHEETS, stylesheets)
                .add(JSON_FONT, page.getFont().toString())
                .add(JSON_COMPONENTS, components)
                .build();
        
        jsonWriter.writeObject(pageObject);
    }
    
    private static JsonArray makeStyleSheetsArray(PageModel page) {
        JsonArrayBuilder jab = Json.createArrayBuilder();
        jab.add(DIR_STYLES + LAYOUT_BASE);
        jab.add(DIR_STYLES + page.getLayout().getStyleSheet());
        jab.add(DIR_STYLES + page.getColorTemplate().getStyleSheet());
        return jab.build();
    }
    
    private static JsonArray makeComponentsArray(PageModel page)
            throws IOException {
        
        JsonArrayBuilder jab = Json.createArrayBuilder();
        List<Component> components = page.getComponents();
        
        for (Component component : components) {
            JsonObject jso = makeComponentObject(component);
            jab.add(jso);
        }
        
        return jab.build();
    }
    
    private static JsonObject makeComponentObject(Component component)
            throws IOException{
        
        JsonObject componentObject;
        
        switch (component.getType()) {
            case HEADING:
                componentObject = makeHeadingObject(component); break;
            case PARAGRAPH:
                componentObject = makeParagraphObject(component); break;
            case LIST:
                componentObject = makeListObject(component); break;
            case IMAGE:
                componentObject = makeImageObject(component); break;
            case VIDEO:
                componentObject = makeVideoObject(component); break;
            default:
                componentObject = makeSlideShowObject(component);
        }
        
        return componentObject;
    }
    
    private static JsonObject makeHeadingObject(Component component) {
        JsonObject headingObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "heading")
                .add(JSON_TEXT, (String) component.getProperties().get(
                        Component.TEXT))
                .build();
        return headingObject;
    }
    
    private static JsonObject makeParagraphObject(Component component) {
        JsonObject paragraphObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "paragraph")
                .add(JSON_FONT, component.getProperties().get(
                        Component.FONT).toString())
                .add(JSON_FONT_SIZE, "" + component.getProperties().get(
                        Component.FONT_SIZE) + "pt")
                .add(JSON_TEXT, (String) component.getProperties().get(
                        Component.TEXT))
                .build();
        return paragraphObject;
    }
    
    private static JsonObject makeListObject(Component component) {
        JsonArray items = makeItemsArray(component);
        JsonObject listObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "list")
                .add(JSON_ITEMS, items)
                .build();
        return listObject;
    }
    
    private static JsonArray makeItemsArray(Component component) {
        List<String> itemList = (List<String>) 
                component.getProperties().get(Component.LIST_ITEMS);
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        for (String item : itemList) {
            jab.add(item);
        }
        
        return jab.build();
    }
    
    private static JsonObject makeImageObject(Component component)
            throws IOException{
        
        String imagePath = (String) component.getProperties().get(
                Component.SOURCE);
        String imageFileName = imagePath.substring(
                imagePath.lastIndexOf('\\') + 1);
        copyImage(imagePath, imageFileName);
        
        JsonObject imageObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "image")
                .add(JSON_SRC, DIR_IMAGES + imageFileName)
                .add(JSON_TEXT, (String) component.getProperties().get(
                        Component.TEXT))
                .add(JSON_WIDTH, "" + component.getProperties().get(
                        Component.WIDTH) + "px")
                .add(JSON_HEIGHT, "" + component.getProperties().get(
                        Component.HEIGHT) + "px")
                .add(JSON_FLOAT, (String) component.getProperties().get(
                        Component.FLOAT))
                .build();
        return imageObject;
    }
    
    private static JsonObject makeVideoObject(Component component)
            throws IOException{
        
        String videoPath = (String) component.getProperties().get(
                Component.SOURCE);
        String videoFileName = videoPath.substring(
                videoPath.lastIndexOf('\\') + 1);
        copyVideo(videoPath, videoFileName);
        
        JsonObject videoObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "video")
                .add(JSON_SRC, DIR_VIDEOS + videoFileName)
                .add(JSON_TEXT, (String) component.getProperties().get(
                        Component.TEXT))
                .add(JSON_WIDTH, "" + component.getProperties().get(
                        Component.WIDTH))
                .add(JSON_HEIGHT, "" + component.getProperties().get(
                        Component.HEIGHT))
                .build();
        return videoObject;
    }
    
    private static void copyVideo(String uri, String fileName)
            throws IOException {
        
        String path = uri.substring(uri.indexOf(':') + 1);
        Path source = Paths.get(path);
        Path target = Paths.get(destination + DIR_VIDEOS + fileName);
        
        if (target.toFile().exists()) return;
        
        Files.copy(source, target);
    }
    
    private static JsonObject makeSlideShowObject(Component component)
            throws IOException {
        
        List<SlideModel> slideList = (List<SlideModel>) component.getProperties().get(
                Component.SLIDES);
        
        JsonArray slides = makeSlidesArray(slideList);
        
        JsonObject slideShowObject = Json.createObjectBuilder()
                .add(JSON_TYPE, "slideshow")
                .add(JSON_SLIDES, slides)
                .build();
        return slideShowObject;
    }
    
    private static JsonArray makeSlidesArray(List<SlideModel> slideList)
            throws IOException {
        
        JsonArrayBuilder jab = Json.createArrayBuilder();
        
        for (SlideModel slide : slideList) {
            JsonObject jso = makeSlideObject(slide);
            jab.add(jso);
        }
        
        return jab.build();
    }
    
    private static JsonObject makeSlideObject(SlideModel slide) throws IOException {
        String imagePath = slide.getImagePath();
        String imageFileName = imagePath.substring(
                imagePath.lastIndexOf('\\') + 1);
        copyImage(imagePath, imageFileName);
        
        JsonObject slideObject = Json.createObjectBuilder()
                .add(JSON_IMAGE_PATH, DIR_IMAGES + imageFileName)
                .add(JSON_CAPTION, slide.getCaption())
                .build();
        return slideObject;
    }
}