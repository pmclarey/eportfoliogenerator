package epg;

/**
 *
 * @author Patrick Clarey
 */
public enum ComponentType {
    SETTINGS,
    HEADING,
    PARAGRAPH,
    LIST,
    IMAGE,
    VIDEO,
    SLIDE_SHOW;
}