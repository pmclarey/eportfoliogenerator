package epg.model;

import epg.ColorTemplate;
import epg.Font;
import epg.Layout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Patrick Clarey
 */
public class PageModel implements Serializable {
    
    // properties for this page
    private String title;
    private Layout layout;
    private ColorTemplate colorTemplate;
    private Font font;

    // this holds all the component models
    List<Component> componentList;
    
    public PageModel(String title, Layout layout, ColorTemplate colorTemplate,
            Font font) {
        
        this.title = title;
        this.layout = layout;
        this.colorTemplate = colorTemplate;
        this.font = font;
        componentList = new ArrayList<>();
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String newTitle) {
        title = newTitle;
        EPortfolioModel.getModel().notifyObservers();
    }
    
    public Layout getLayout() {
        return layout;
    }
    
    public void setLayout(Layout newLayout) {
        layout = newLayout;
    }
    
    public ColorTemplate getColorTemplate() {
        return colorTemplate;
    }
    
    public void setColorTemplate(ColorTemplate newColorTemplate) {
        colorTemplate = newColorTemplate;
    }
    
    public Font getFont() {
        return font;
    }
    
    public void setFont(Font newFont) {
        font = newFont;
    }
    
    public List<Component> getComponents() {
        return componentList;
    }
    
    public void addComponent(Component component) {
        componentList.add(component);
    }
}