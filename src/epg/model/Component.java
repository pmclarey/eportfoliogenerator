package epg.model;

import epg.ColorTemplate;
import epg.ComponentType;
import static epg.Constants.IMAGE_DEFAULT;
import static epg.Constants.IMAGE_DEFAULT_BANNER;
import static epg.Constants.PATH_IMAGES;
import static epg.Constants.PATH_VIDEOS;
import static epg.Constants.VIDEO_DEFAULT;
import epg.Font;
import epg.Layout;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 *
 * @author Patrick Clarey
 */
public class Component implements Serializable {
    
    // constants for retrieving properties
    public static final String BANNER_IMAGE_PATH = "bannerImagePath";
    public static final String NAME = "name";
    public static final String DEFAULT_LAYOUT = "defaultLayout";
    public static final String DEFAULT_COLOR_TEMPLATE = "defaultColorTemplate";
    public static final String DEFAULT_FONT = "defaultFont";
    public static final String FOOTER = "footer";
    public static final String TEXT = "text";
    public static final String FONT = "font";
    public static final String FONT_SIZE = "fontSize";
    public static final String LIST_ITEMS = "items";
    public static final String SOURCE = "src";
    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String FLOAT = "float";
    public static final String SLIDES = "slides";
    
    // this specifies the type of component (heading, paragraph, image, etc.)
    private final ComponentType TYPE;
    
    // data structure holds the properties, which vary by type
    private Map<String, Object> properties;
    
    public Component(ComponentType type) {
        this.TYPE = type;
        properties = new TreeMap<>();
        
        switch(type) {
            case SETTINGS:
                initSettings(); break;
            case HEADING:
                initHeading(); break;
            case PARAGRAPH:
                initParagraph(); break;
            case LIST:
                initList(); break;
            case IMAGE:
                initImage(); break;
            case VIDEO:
                initVideo(); break;
            default:
                initSlideShow();
        }
    }
    
    public ComponentType getType() {
        return TYPE;
    }
    
    public Map getProperties() {
        return properties;
    }
    
    public void setProperties(Map newProperties) {
        properties = newProperties;
    }
    
    private void initSettings() {
        properties.put(BANNER_IMAGE_PATH,
                "file:" + PATH_IMAGES + IMAGE_DEFAULT_BANNER);
        properties.put(NAME, "");
        properties.put(DEFAULT_LAYOUT, Layout.NAV_TOP);
        properties.put(DEFAULT_COLOR_TEMPLATE, ColorTemplate.DARK);
        properties.put(DEFAULT_FONT, Font.DROID_SERIF);
        properties.put(FOOTER, "");
    }
    
    private void initHeading() {
        properties.put(TEXT, "");
    }
    
    private void initParagraph() {
        properties.put(FONT, Font.DROID_SERIF);
        properties.put(FONT_SIZE, 12);
        properties.put(TEXT, "");
    }
    
    private void initList() {
        properties.put(LIST_ITEMS, new ArrayList<>());
    }
    
    private void initImage() {
        properties.put(SOURCE, "file:" + PATH_IMAGES + IMAGE_DEFAULT);
        properties.put(TEXT, "");
        properties.put(WIDTH, 600.0);
        properties.put(HEIGHT, 300.0);
        properties.put(FLOAT, "Do not float");
    }
    
    private void initVideo() {
        properties.put(SOURCE, PATH_VIDEOS + VIDEO_DEFAULT);
        properties.put(TEXT, "");
        properties.put(WIDTH, 800.0);
        properties.put(HEIGHT, 300.0);
    }
    
    private void initSlideShow() {
        properties.put(SLIDES, new ArrayList<>());
    }
    
    @Override
    public String toString() {
        switch(TYPE) {
            case HEADING:
                return String.format(
                        "%-14s%-66s", "Heading" , properties.get(TEXT));
            case PARAGRAPH:
                return String.format(
                        "%-14s%-66s", "Paragraph" ,
                        new Scanner((String) properties.get(TEXT)).nextLine());
            case LIST:
                return String.format(
                        "%-14s%-66s", "List",
                            ((List) properties.get(LIST_ITEMS)).get(0));
            case IMAGE:
                String imageFile = (String) properties.get(SOURCE);
                imageFile = imageFile.substring(
                        imageFile.lastIndexOf('\\') + 1);
                return String.format("%-14s%-66s", "Image", imageFile + " [" +
                        properties.get(WIDTH) + "x" + properties.get(HEIGHT) +
                        "]");
            case VIDEO:
                String videoFile = (String) properties.get(SOURCE);
                videoFile = videoFile.substring(
                        videoFile.lastIndexOf('\\') + 1);
                return String.format("%-14s%-66s", "Video", videoFile + " [" +
                        properties.get(WIDTH) + "x" + properties.get(HEIGHT) +
                        "]");
            default:
                return "Slide Show";
        }
    }
}