package epg.model;

import epg.ColorTemplate;
import epg.ComponentType;
import epg.Font;
import epg.Layout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Patrick Clarey
 */
public final class EPortfolioModel implements Serializable {

    // singleton instance of ePortfolio currently being edited
    private static EPortfolioModel model;
    
    // settings for this ePortfolio
    private Component settings;
    
    // this holds all the page models
    private final List<PageModel> PAGE_LIST;
    
    // this determines name of page during creation
    private int pageCounter;
    
    // true if ePortfolio has not been changed since last save, false otherwise
    private static boolean saved;
    
    // these observe this model, and need to be notified of changes
    private static List<ModelObserver> registeredObservers;
    
    // this is the title, which serves as a file name
    private String title;
    
    private EPortfolioModel() {
        settings = new Component(ComponentType.SETTINGS);
        PAGE_LIST = new ArrayList<>();
        saved = false;
        title = "";
    }
    
    public static EPortfolioModel getModel() {
        if (model == null) {
            model = new EPortfolioModel();
            model.notifyObservers();
        }
        
        return model;
    }
    
    public void addPage() {
        String title;
        
        if (++pageCounter == 1) {
            title = "Welcome";
        }
        else {
            title = "Page " + pageCounter;
        }
        
        Map defaults = settings.getProperties();
        Layout layout = (Layout) defaults.get(Component.DEFAULT_LAYOUT);
        ColorTemplate colorTemplate = (ColorTemplate) defaults.get(
                Component.DEFAULT_COLOR_TEMPLATE);
        Font font = (Font) defaults.get(Component.DEFAULT_FONT);
                
        PageModel newPage = new PageModel(title, layout, colorTemplate, font);
        PAGE_LIST.add(newPage);
        saved = false;
        notifyObservers();
    }
    
    public boolean removePage(PageModel pageToRemove) {
        if (PAGE_LIST.contains(pageToRemove)) {
            PAGE_LIST.remove(pageToRemove);
            saved = false;
            notifyObservers();
            return true;
        }
        
        // else
        return false;
    }
    
    public boolean addComponent(Component component, PageModel page) {
        if (PAGE_LIST.contains(page)) {
            page.addComponent(component);
            saved = false;
            notifyObservers();
            return true;
        }
        
        // else
        return false;
    }
    
    public boolean editComponent(
            Component oldComponent, Component newComponent, PageModel page) {
        
        if (PAGE_LIST.contains(page) &&
                page.componentList.contains(oldComponent)) {
            
            page.componentList.set(page.componentList.indexOf(oldComponent),
                    newComponent);
            saved = false;
            notifyObservers();
            return true;
        }
        
        // else
        return false;
    }
    
    public boolean removeComponent(Component component, PageModel page) {
        if (PAGE_LIST.contains(page) &&
                page.componentList.contains(component)) {
            
            page.componentList.remove(component);
            saved = false;
            notifyObservers();
            return true;
        }
        
        // else
        return false;
    }
    
    public static void resetModel() {
        model = new EPortfolioModel();
        model.notifyObservers();
    }
    
    public static void loadModel(EPortfolioModel loadedModel) {
        model = loadedModel;
        saved = true;
        model.notifyObservers();
    }
    
    public Component getSettings() {
        return settings;
    }
    
    public void setSettings(Component newSettings) {
        settings = newSettings;
        saved = false;
        notifyObservers();
    }
    
    public List<PageModel> getPages() {
        return PAGE_LIST;
    }
    
    public static boolean isLoaded() {
        return model != null;
    }
    
    public static boolean isSavedSinceLastModified() {
        return saved;
    }
    
    public static void registerObserver(ModelObserver newObserver) {
        if (registeredObservers == null) {
            registeredObservers = new ArrayList<>();
        }
        
        registeredObservers.add(newObserver);
    }
    
    void notifyObservers() {
        if (registeredObservers == null) {
            return;
        }
        
        registeredObservers.stream().forEach((observer) -> {
            observer.processModelChanged();
        });
    }
    
    public String getTitle() {
        return model.title;
    }
    
    public void setTitle(String newTitle) {
        model.title = newTitle;
    }
    
    public static void markSaved() {
        saved = true;
        model.notifyObservers();
    }
    
    public static void markUnsaved() {
        saved = false;
        model.notifyObservers();
    }
}