package epg.model;

/**
 *
 * @author Patrick Clarey
 */
public interface ModelObserver {
    abstract void processModelChanged();
}