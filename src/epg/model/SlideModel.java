package epg.model;

import static epg.Constants.IMAGE_DEFAULT;
import static epg.Constants.PATH_IMAGES;
import java.io.Serializable;

/**
 *
 * @author Patrick
 */
public class SlideModel implements Serializable {
    private String imagePath;
    private String caption;
    
    public SlideModel() {
        imagePath = "file:" + PATH_IMAGES + IMAGE_DEFAULT;
        caption = "";
    }
    
    public String getImagePath() {
        return imagePath;
    }
    
    public void setImagePath(String newImagePath) {
        imagePath = newImagePath;
    }
    
    public String getCaption() {
        return caption;
    }
    
    public void setCaption(String newCaption) {
        caption = newCaption;
    } 
}
