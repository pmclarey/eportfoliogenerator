package epg;

import static epg.Constants.STRING_DANCING_SCRIPT;
import static epg.Constants.STRING_DROID_SERIF;
import static epg.Constants.STRING_INCONSOLATA;
import static epg.Constants.STRING_INDIE_FLOWER;
import static epg.Constants.STRING_OPEN_SANS;

/**
 *
 * @author Patrick Clarey
 */
public enum Font {
    DROID_SERIF,
    OPEN_SANS,
    INDIE_FLOWER,
    DANCING_SCRIPT,
    INCONSOLATA;
    
    @Override
    public String toString() {
        switch(this) {
            case DROID_SERIF:
                return STRING_DROID_SERIF;
            case OPEN_SANS:
                return STRING_OPEN_SANS;
            case INDIE_FLOWER:
                return STRING_INDIE_FLOWER;
            case DANCING_SCRIPT:
                return STRING_DANCING_SCRIPT;
            default:
                return STRING_INCONSOLATA; 
        }
    }
}