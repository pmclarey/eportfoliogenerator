package epg;

import epg.view.AppView;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 *
 * @author Patrick Clarey
 */
public class EPortfolioGenerator extends Application {
    
    @Override
    public void start(Stage stage) {
        AppView ui = new AppView();
        ui.initUI(stage);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}