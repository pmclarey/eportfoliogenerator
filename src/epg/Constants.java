package epg;

/**
 *
 * @author Patrick Clarey
 */
public class Constants {
    
    // location for temporary web site loaded in site viewer mode
    public static final String PATH_TEMP = "./temp/";
    public static final String PATH_SITES = "./sites/";
    public static final String INDEX_HTML = "index.html";
    
    // String descriptions for layout templates
    public static final String STRING_NAV_TOP =
            "Navigation bar on top";
    public static final String STRING_NAV_UNDER =
            "Navigation bar under banner";
    public static final String STRING_NAV_UNDER_BANNER_IMG_NONE =
            "Navigation bar under banner (no banner image)";
    public static final String STRING_NAV_FLOAT =
            "Navigation menu floated to left";
    public static final String STRING_NAV_FLOAT_BANNER_IMG_NONE =
            "Navigation menu floated to left (no banner image)";
    
    // style sheets for layout templates
    public static final String STYLE_SHEET_NAV_TOP =
            "layout_nav_top.css";
    public static final String STYLE_SHEET_NAV_UNDER =
            "layout_nav_under.css";
    public static final String STYLE_SHEET_NAV_UNDER_BANNER_IMG_NONE =
            "layout_nav_under_banner_img_none.css";
    public static final String STYLE_SHEET_NAV_FLOAT =
            "layout_nav_float.css";
    public static final String STYLE_SHEET_NAV_FLOAT_BANNER_IMG_NONE =
            "layout_nav_float_banner_img_none.css";
    
    // String descriptions for color templates
    public static final String STRING_LIGHT = "Light";
    public static final String STRING_DARK = "Dark";
    public static final String STRING_SEA_WOLF = "Sea Wolf";
    public static final String STRING_BLUE_GRAY = "Blue Gray";
    public static final String STRING_WOODLAND = "Woodland";
    
    // style sheets for color templates
    public static final String STYLE_SHEET_LIGHT = "theme_light.css";
    public static final String STYLE_SHEET_DARK = "theme_dark.css";
    public static final String STYLE_SHEET_SEA_WOLF = "theme_sea_wolf.css";
    public static final String STYLE_SHEET_BLUE_GRAY = "theme_blue_gray.css";
    public static final String STYLE_SHEET_WOODLAND = "theme_woodland.css";
    
    // String descriptions for fonts
    public static final String STRING_DROID_SERIF = "'Droid Serif', serif";
    public static final String STRING_OPEN_SANS = "'Open Sans', sans-serif";
    public static final String STRING_INDIE_FLOWER = "'Indie Flower', fantasy";
    public static final String STRING_DANCING_SCRIPT =
            "'Dancing Script', cursive";
    public static final String STRING_INCONSOLATA = "'Inconsolata', monospace";
    
    // application window constants 
    public static final String APP_TITLE = "The ePortfolio Generator";
    public static final double INITIAL_WIDTH_TO_FULLSCREEN_RATIO = 0.75;
    public static final double INITIAL_HEIGHT_TO_FULLSCREEN_RATIO = 0.75;
    
    // style constants
    public static final String PATH_STYLES = "/epg/styles/";
    public static final String STYLE_SHEET = "styles.css";
    public static final String STYLE_CLASS_TOOL_BAR_BUTTON = "tool_bar_button";
    public static final String STYLE_ID_SITE_TOOL_BAR = "site_tool_bar";
    
    // label constants
    public static final String LABEL_NEW = "New";
    public static final String LABEL_LOAD = "Load";
    public static final String LABEL_SAVE = "Save";
    public static final String LABEL_SAVE_AS = "Save As";
    public static final String LABEL_EXPORT = "Export";
    public static final String LABEL_EXIT = "Exit";
    public static final String LABEL_ADD = "Add Page";
    public static final String LABEL_REMOVE = "Remove Page";
    public static final String LABEL_EDITOR = "Page Editor";
    public static final String LABEL_VIEWER = "Site Viewer";
    public static final String LABEL_BANNER_IMAGE = 
            "Click to change banner image:";
    public static final String LABEL_NAME =
            "Enter the ePortfolio owner's name:";
    public static final String LABEL_DEFAULT_LAYOUT =
            "Select a default layout:";
    public static final String LABEL_DEFAULT_COLOR_TEMPLATE =
            "Select a default color template:";
    public static final String LABEL_DEFAULT_FONT =
            "Select a default font:";
    public static final String LABEL_FOOTER =
            "(Optional) Enter a footer to be displayed on each page:";
    public static final String LABEL_PAGE_LAYOUT = "Page layout:";
    public static final String LABEL_PAGE_COLOR_TEMPLATE =
            "Page color template:";
    public static final String LABEL_PAGE_FONT =
            "Page font:";
    
    // icon constants
    public static final String PATH_ICONS = "./icons/";
    public static final String ICON_APP = "Briefcase-24.png";
    public static final String ICON_NEW = "glyphicons-10-magic.png";
    public static final String ICON_LOAD = "glyphicons-145-folder-open.png";
    public static final String ICON_SAVE = "glyphicons-444-floppy-disk.png";
    public static final String ICON_SAVE_AS = "glyphicons-444-floppy-disk.png";
    public static final String ICON_EXPORT = "glyphicons-420-disk-export.png";
    public static final String ICON_EXIT = "glyphicons-389-exit.png";
    public static final String ICON_ADD_PAGE = "glyphicons-319-more-items.png";
    public static final String ICON_REMOVE_PAGE =
            "glyphicons-193-circle-remove.png";
    public static final String ICON_PAGE = "glyphicons-37-file.png";
    public static final String ICON_EDITOR = "glyphicons-151-edit.png";
    public static final String ICON_VIEWER = "glyphicons-52-eye-open.png";
    public static final String ICON_PROPERTIES = "glyphicons-281-settings.png";
    public static final String ICON_EDIT = "glyphicons-31-pencil.png";
    public static final String ICON_REMOVE_COMPONENT =
            "glyphicons-198-remove.png";
    public static final String ICON_HEADING = "glyphicons-460-header.png";
    public static final String ICON_PARAGRAPH = "glyphicons-111-align-left.png";
    public static final String ICON_LIST = "glyphicons-115-list.png";
    public static final String ICON_IMAGE = "glyphicons-139-picture.png";
    public static final String ICON_VIDEO = "glyphicons-9-film.png";
    public static final String ICON_SLIDE_SHOW =
            "glyphicons-479-blackboard.png";
    public static final String ICON_HYPERLINK = "glyphicons-51-link.png";
    public static final String ICON_ADD_ITEM = "glyphicons-191-circle-plus.png";
    public static final String ICON_REMOVE_ITEM =
            "glyphicons-193-circle-remove.png";
    public static final String ICON_MOVE_UP =
            "glyphicons-219-circle-arrow-top.png";
    public static final String ICON_MOVE_DOWN =
            "glyphicons-220-circle-arrow-down.png";
    public static final String ICON_OK = "glyphicons-207-ok-2.png";
    public static final String ICON_CANCEL = "glyphicons-208-remove-2.png";
    
    // image constants
    public static final String PATH_IMAGES = "./images/";
    public static final String IMAGE_DEFAULT_BANNER = "default-banner.png";
    public static final String IMAGE_DEFAULT = "default-image.png";
    
    // video constants
    public static final String PATH_VIDEOS = "./videos/";
    public static final String VIDEO_DEFAULT = "blank.mp4";
}