package epg.controller;

import epg.ColorTemplate;
import epg.ComponentType;
import epg.Font;
import epg.Layout;
import epg.model.Component;
import epg.model.EPortfolioModel;
import epg.model.PageModel;
import epg.view.AppView;
import epg.view.HeadingDialog;
import epg.view.ImageDialog;
import epg.view.ListDialog;
import epg.view.ParagraphDialog;
import epg.view.SettingsDialog;
import epg.view.SlideShowDialog;
import epg.view.VideoDialog;

/**
 *
 * @author Patrick Clarey
 */
public class PageEditorController {
    AppView ui;
    
    public PageEditorController(AppView ui) {
        this.ui = ui;
    }
    
    public void handleChangeProperties() {
        EPortfolioModel ePortfolio = EPortfolioModel.getModel();
        
        Component newProperties = 
                new SettingsDialog(ePortfolio.getSettings()).getComponent();
        
        if (newProperties == null) return;
        
        ePortfolio.setSettings(newProperties);
    }
    
    public void handleChangeTitle(String newTitle, PageModel page) {
        page.setTitle(newTitle);
        EPortfolioModel.markUnsaved();
    }
    
    public void handleChangeLayout(Layout newLayout, PageModel page) {
        page.setLayout(newLayout);
        EPortfolioModel.markUnsaved();
    }
    
    public void handleChangeColorTemplate(ColorTemplate newColorTemplate,
            PageModel page) {
        
        page.setColorTemplate(newColorTemplate);
        EPortfolioModel.markUnsaved();
    }
    
    public void handleChangeFont(Font newFont, PageModel page) {
        page.setFont(newFont);
        EPortfolioModel.markUnsaved();
    }
    
    public void handleAddComponent(ComponentType type, PageModel page) {
        Component newComponent;
        
        switch(type) {
            case HEADING:
                newComponent = new HeadingDialog().getComponent(); break;
            case PARAGRAPH:
                newComponent = new ParagraphDialog(page).getComponent(); break;
            case LIST:
                newComponent = new ListDialog().getComponent(); break;
            case IMAGE:
                newComponent = new ImageDialog().getComponent(); break;
            case VIDEO:
                newComponent = new VideoDialog().getComponent(); break;
            default:
                newComponent = new SlideShowDialog().getComponent();
        }
        
        if (newComponent != null) {
            EPortfolioModel.getModel().addComponent(newComponent, page);
        }
    }
    
    public void handleEditComponent(Component oldComponent, PageModel page) {
        Component newComponent;
        
        switch(oldComponent.getType()) {
            case HEADING:
                newComponent = new HeadingDialog(oldComponent).getComponent();
                break;
            case PARAGRAPH:
                newComponent = new ParagraphDialog(oldComponent).getComponent();
                break;
            case LIST:
                newComponent = new ListDialog(oldComponent).getComponent();
                break;
            case IMAGE:
                newComponent = new ImageDialog(oldComponent).getComponent();
                break;
            case VIDEO:
                newComponent = new VideoDialog(oldComponent).getComponent();
                break;
            default:
                newComponent = new SlideShowDialog(oldComponent).getComponent();
        }
        
        if (newComponent != null) {
            EPortfolioModel.getModel().editComponent(oldComponent, newComponent,
                    page);
        }
    }
    
    public void handleRemoveComponent(Component component, PageModel page) {
        EPortfolioModel.getModel().removeComponent(component, page);
    }
}