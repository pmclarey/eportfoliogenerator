package epg.controller;

import static epg.Constants.PATH_TEMP;
import epg.file.Exporter;
import epg.view.AppView;
import java.io.IOException;

/**
 *
 * @author Patrick Clarey
 */
public class ModeController {
    AppView ui;
    
    public ModeController(AppView ui) {
        this.ui = ui;
    }
    
    public void handleModeChanged(boolean viewerSelected) {
        if (viewerSelected) {
            
            try {
                Exporter.export(PATH_TEMP);
            }
            catch (IOException ioe) {
                ioe.printStackTrace();
            }
            
            ui.loadWebEngine();
        }
    }
}