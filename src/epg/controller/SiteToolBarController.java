package epg.controller;

import epg.model.EPortfolioModel;
import epg.model.PageModel;
import epg.view.AppView;
import javafx.scene.control.Toggle;

/**
 *
 * @author Patrick Clarey
 */
public class SiteToolBarController {
    AppView ui;
    
    public SiteToolBarController(AppView ui) {
        this.ui = ui;
    }
    
    public void handleAdd() {
        EPortfolioModel.getModel().addPage();
    }
    
    public void handleRemove(PageModel page) {
        EPortfolioModel.getModel().removePage(page);
    }
    
    public void handleSelect(Toggle page) {
        if (page != null) {
            ui.loadPage(page);
        }
    }
}