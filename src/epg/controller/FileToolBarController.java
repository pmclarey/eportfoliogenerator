package epg.controller;

import static epg.Constants.PATH_SITES;
import epg.file.Exporter;
import epg.file.FileManager;
import epg.model.Component;
import epg.model.EPortfolioModel;
import epg.view.AppView;
import epg.view.SettingsDialog;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;

/**
 *
 * @author Patrick Clarey
 */
public class FileToolBarController {
    AppView ui;
    
    ButtonType buttonSave = new ButtonType("Save");
    ButtonType buttonCont = new ButtonType("Continue without saving");
    ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
    
    public FileToolBarController(AppView ui) {
        this.ui = ui;
    }
    
    public void handleNew() {
        
        if (EPortfolioModel.isLoaded() &&
                !EPortfolioModel.isSavedSinceLastModified()) {
            
            ButtonType result = showSaveConfirmationDialog();
            
            if (result == null || result == buttonCancel) {
                return;
            }
            else if (result == buttonSave){
                handleSave();
            }
        }
        
        Component newEPortfolioProperties = 
                new SettingsDialog().getComponent();
        
        if (newEPortfolioProperties == null) return;
        
        EPortfolioModel.resetModel();
        EPortfolioModel ePortfolio = EPortfolioModel.getModel();
        ePortfolio.setSettings(newEPortfolioProperties);
        ePortfolio.addPage();
        ui.switchToEditor();
    }
    
    public void handleLoad() {
        
        if (EPortfolioModel.isLoaded() &&
                !EPortfolioModel.isSavedSinceLastModified()) {
            
            ButtonType result = showSaveConfirmationDialog();
            
            if (result == null || result == buttonCancel) {
                return;
            }
            else if (result == buttonSave){
                handleSave();
            }
        }
        
        MediaSelector saveSelector = new MediaSelector(ui.stage);
        File fileToOpen = saveSelector.getSavedFromUser();
        
        if (fileToOpen == null) return;
        
        try {
            FileManager.load(fileToOpen);
        }
        catch (IOException | ClassNotFoundException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "An error occurred while loading.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        ui.switchToEditor();
    }
    
    public void handleSave() {
        String title = EPortfolioModel.getModel().getTitle();
        
        if (title.equals("")) {
            title = FileManager.getTitleFromUser();
        }
        
        if (title == null) return;
        
        try {
            FileManager.save(title);
        }
        catch (IOException ioe) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "An error occurred while saving.");
            alert.setHeaderText("");
            alert.showAndWait();
        }
    }
    
    public void handleSaveAs() {
        String newTitle = FileManager.getTitleFromUser();
        
        if (newTitle == null) return;
        
        handleSave();
    }
    
    public void handleExport() {
        String destination = PATH_SITES + 
                EPortfolioModel.getModel().getSettings().getProperties().get(
                        Component.NAME) + "/";
        destination = destination.replaceAll(" ", "_");
        
        try {
            Exporter.export(destination);
        }
        catch (IOException ioe) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "An error occurred while exporting.");
            alert.setHeaderText("");
            alert.showAndWait(); return;
        }
        
        ui.switchToViewer();
    }
    
    public void handleExit() {
        
        if (EPortfolioModel.isLoaded() &&
                !EPortfolioModel.isSavedSinceLastModified()) {
            
            ButtonType result = showSaveConfirmationDialog();
            
            if (result == null || result == buttonCancel) {
                return;
            }
            else if (result == buttonSave){
                handleSave();
            }
        }
        
        Platform.exit();
    }
    
    private ButtonType showSaveConfirmationDialog() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("The current ePortfolio has not been saved.");
        alert.setContentText("Do you want to proceed?");
        alert.getButtonTypes().setAll(buttonSave, buttonCont, buttonCancel);
        
        Optional<ButtonType> result = alert.showAndWait();
        
        if (!result.isPresent()) return null;
        
        return result.get();
    }
}