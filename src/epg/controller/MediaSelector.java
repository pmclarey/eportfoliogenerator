package epg.controller;

import java.io.File;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

/**
 *
 * @author Patrick Clarey
 */
public class MediaSelector {
    FileChooser fileChooser;
    Window ownerWindow;
    
    public MediaSelector(Window ownerWindow) {
        fileChooser = new FileChooser();
        this.ownerWindow = ownerWindow;
    }
    
    public String getImageFromUser() {
        fileChooser.setTitle("Select Image File");
        fileChooser.getExtensionFilters().add(
                new ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        return getFilePath();
    }
    
    public String getVideoFromUser() {
        fileChooser.setTitle("Select Video File");
        fileChooser.getExtensionFilters().add(
                new ExtensionFilter("Video Files", "*.mp4"));
        return getFilePath();
    }
    
    public File getSavedFromUser() {
        fileChooser.setTitle("Select Saved ePortfolio");
        fileChooser.getExtensionFilters().add(new ExtensionFilter(
                "ePortfolios", "*.epf"));
        fileChooser.setInitialDirectory(new File("./saved"));
        return fileChooser.showOpenDialog(ownerWindow);
    }
    
    private String getFilePath() {
        File file = fileChooser.showOpenDialog(ownerWindow);
        
        if (file == null) return null;
        
        return file.getPath();
    }
}