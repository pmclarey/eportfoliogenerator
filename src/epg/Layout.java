package epg;

import static epg.Constants.STRING_NAV_FLOAT;
import static epg.Constants.STRING_NAV_FLOAT_BANNER_IMG_NONE;
import static epg.Constants.STRING_NAV_TOP;
import static epg.Constants.STRING_NAV_UNDER;
import static epg.Constants.STRING_NAV_UNDER_BANNER_IMG_NONE;
import static epg.Constants.STYLE_SHEET_NAV_FLOAT;
import static epg.Constants.STYLE_SHEET_NAV_FLOAT_BANNER_IMG_NONE;
import static epg.Constants.STYLE_SHEET_NAV_TOP;
import static epg.Constants.STYLE_SHEET_NAV_UNDER;
import static epg.Constants.STYLE_SHEET_NAV_UNDER_BANNER_IMG_NONE;

/**
 *
 * @author Patrick Clarey
 */
public enum Layout {
    NAV_TOP,
    NAV_UNDER,
    NAV_UNDER_BANNER_IMG_NONE,
    NAV_FLOAT,
    NAV_FLOAT_BANNER_IMG_NONE;
    
    public String getStyleSheet() {
        switch(this) {
            case NAV_TOP:
                return STYLE_SHEET_NAV_TOP;
            case NAV_UNDER:
                return STYLE_SHEET_NAV_UNDER;
            case NAV_UNDER_BANNER_IMG_NONE:
                return STYLE_SHEET_NAV_UNDER_BANNER_IMG_NONE;
            case NAV_FLOAT:
                return STYLE_SHEET_NAV_FLOAT;
            default:
                return STYLE_SHEET_NAV_FLOAT_BANNER_IMG_NONE;
        }
    }
    
    @Override
    public String toString() {
        switch(this) {
            case NAV_TOP:
                return STRING_NAV_TOP;
            case NAV_UNDER: 
                return STRING_NAV_UNDER;
            case NAV_UNDER_BANNER_IMG_NONE: 
                return STRING_NAV_UNDER_BANNER_IMG_NONE;
            case NAV_FLOAT:
                return STRING_NAV_FLOAT;
            default:
                return STRING_NAV_FLOAT_BANNER_IMG_NONE;
        }
    }
}