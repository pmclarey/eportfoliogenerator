package epg;

import static epg.Constants.STRING_BLUE_GRAY;
import static epg.Constants.STRING_DARK;
import static epg.Constants.STRING_LIGHT;
import static epg.Constants.STRING_SEA_WOLF;
import static epg.Constants.STRING_WOODLAND;
import static epg.Constants.STYLE_SHEET_BLUE_GRAY;
import static epg.Constants.STYLE_SHEET_DARK;
import static epg.Constants.STYLE_SHEET_LIGHT;
import static epg.Constants.STYLE_SHEET_SEA_WOLF;
import static epg.Constants.STYLE_SHEET_WOODLAND;

/**
 *
 * @author Patrick Clarey
 */
public enum ColorTemplate {
    LIGHT,
    DARK,
    SEA_WOLF,
    BLUE_GRAY,
    WOODLAND;
    
    @Override
    public String toString() {
        switch(this) {
            case LIGHT:
                return STRING_LIGHT;
            case DARK:
                return STRING_DARK;
            case SEA_WOLF:
                return STRING_SEA_WOLF;
            case BLUE_GRAY:
                return STRING_BLUE_GRAY;
            default:
                return STRING_WOODLAND;
        }
    }
    
    public String getStyleSheet() {
        switch(this) {
            case LIGHT:
                return STYLE_SHEET_LIGHT;
            case DARK:
                return STYLE_SHEET_DARK;
            case SEA_WOLF:
                return STYLE_SHEET_SEA_WOLF;
            case BLUE_GRAY:
                return STYLE_SHEET_BLUE_GRAY;
            default:
                return STYLE_SHEET_WOODLAND;
        }
    }
}